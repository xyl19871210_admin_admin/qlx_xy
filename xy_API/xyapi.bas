#Include Once "Windows.bi"
#Include "win\winnt.bi"
#include "win\iphlpapi.bi"
#Include "win\tlhelp32.bi"
#Include "Win\Psapi.bi"
#Include "Win\commctrl.bi"
#Include "Crt\String.bi"
#LibPath "Lib"



#Define xMQ_Tool_DebugMode



#Include "Inc\Moudle\xrtl.bi"
#Include "Inc\Moudle\xBuffer.bi"
#Include "Inc\Moudle\xWait.bi"
#Include "Inc\Moudle\CrypToHash.bi"
'#Include "Inc\Moudle\xywhEncry1.bi"
#Include "Inc\Moudle\NtDef.bi"
#Include "Inc\Moudle\Iocp.bi"
#Include "Inc\Moudle\shlwapi.bi"



Dim Shared AutoFreeBuffer As xBuffer
Dim Shared hInst As HINSTANCE
Dim Shared Global_Sys_Info As SYSTEM_INFO
Dim Shared Global_Sys_Bits As Integer
Dim Shared Global_Wsk_Init As Integer
Dim Shared Global_Hdr_Icmp As HANDLE
Dim Shared Global_Paint_Font As HFONT



#Include "Inc\Window\spy.bi"
#Include "Inc\Window\Paint.bi"
#Include "Inc\Window\MenuWin.bi"
#Include "Inc\Window\TreeView.bi"



#Include "Inc\Export\http.bi"
#Include "Inc\Export\charset.bi"
#Include "Inc\Export\Network.bi"
#Include "Inc\Export\dialog.bi"
#Include "Inc\Export\ini.bi"
#Include "Inc\Export\cryptohash.bi"
#Include "Inc\Export\file.bi"
#Include "Inc\Export\base.bi"
#Include "Inc\Export\sysinfo.bi"
#Include "Inc\Export\process.bi"
#Include "Inc\Export\thread.bi"
#Include "Inc\Export\window.bi"
#Include "Inc\Export\clipboard.bi"
#Include "Inc\Export\spycon.bi"
#Include "Inc\Export\paintcon.bi"
#Include "Inc\Export\hook.bi"
#Include "Inc\Export\udp.bi"
#Include "Inc\Export\tcp_client.bi"
#Include "Inc\Export\tcp_server.bi"
#Include "Inc\Export\menu.bi"
'#Include "Inc\Export\log.bi"
#Include "Inc\Export\treeview.bi"
#Include "Inc\Export\listview.bi"
#Include "Inc\Export\mouse.bi"
#Include "Inc\Export\keyboard.bi"
#Include "Inc\Export\lock.bi"



Sub xyLib_Init(hdr As HANDLE)
	hInst = hdr
	InitCommonControls()
	Randomize(Timer(), 1)
	Dim NtHdr As HANDLE = GetModuleHandle("ntdll")
	If NtHdr Then
		NtQueryInformationProcess = Cast(Any Ptr, GetProcAddress(NtHdr, "NtQueryInformationProcess"))
		NtWow64QueryInformationProcess64 = Cast(Any Ptr, GetProcAddress(NtHdr, "NtWow64QueryInformationProcess64"))
		NtReadVirtualMemory = Cast(Any Ptr, GetProcAddress(NtHdr, "NtReadVirtualMemory"))
		NtWow64ReadVirtualMemory64 = Cast(Any Ptr, GetProcAddress(NtHdr, "NtWow64ReadVirtualMemory64"))
		ZwSuspendProcess = Cast(Any Ptr, GetProcAddress(NtHdr, "ZwSuspendProcess"))
		ZwResumeProcess = Cast(Any Ptr, GetProcAddress(NtHdr, "ZwResumeProcess"))
	EndIf
	Proc_IsWow64Process = Cast(Any Ptr, GetProcAddress(GetModuleHandle("kernel32.dll"), "IsWow64Process"))
	Global_Sys_Bits = xSys_Bits()
	' ��ʼ��WinSock
	Dim wsaData As WSADATA
	Global_Wsk_Init = WSAStartup(MAKEWORD(2, 2), @wsaData)
	' ��ʼ�� ICMP
	Global_Hdr_Icmp = LoadLibrary("icmp.dll")
	If Global_Hdr_Icmp Then
		IcmpCreateFile = Cast(Any Ptr, GetProcAddress(Global_Hdr_Icmp, "IcmpCreateFile"))
		IcmpCloseHandle = Cast(Any Ptr, GetProcAddress(Global_Hdr_Icmp, "IcmpCloseHandle"))
		IcmpSendEcho = Cast(Any Ptr, GetProcAddress(Global_Hdr_Icmp, "IcmpSendEcho"))
	EndIf
End Sub

Sub xyLib_Unit()
	WSACleanup()
	FreeLibrary(Global_Hdr_Icmp)
	If Global_Paint_Font Then
		DeleteObject(Global_Paint_Font)
	EndIf
	xRtl.FreeMemory()
	AutoFreeBuffer.FreeMemory()
End Sub



#If __FB_OUT_EXE__
	
	xyLib_Init(GetModuleHandle(NULL))
	
	'Print AdjustPrivilege()
	'Print PID_InjectDll(8408, "C:\Users\Administrator\Desktop\Optimize\Optimize\Release\Optimize.dll")
	
	'Print *Dlg_OpenFiles(0, "c:\*", "*|*||")
	
	
	
	Print *RunApp_Console("cmd /?", 5)
	Print *RunApp_Console("cmd /C dir", 5)
	
	
	
	/'
	Dim hTree As HWND = Cast(HWND, 329500)
	
	Dim iCount As Integer = TreeView_Count(hTree)
	Dim hNode As HTREEITEM = TreeView_RootNode(hTree)
	For i As Integer = 1 To iCount
		Print *TreeView_GetText(hTree, hNode)
		hNode = TreeView_GetNextList(hTree, hNode)
	Next
	
	hNode = TreeView_GetSelNode(hTree)
	Print *TreeView_GetText(hTree, hNode)
	
	TreeView_ClickNode(hTree, TreeView_RootNode(hTree))
	
	Print WC_TREEVIEW
	'/
	
	Sleep
	xyLib_Unit()
	
#ElseIf __FB_OUT_DLL__
	
	Function DllMain Alias "DllMain" (hinstDLL As HINSTANCE, fdwReason As DWORD, lpvReserved As LPVOID) As BOOL
		Select Case fdwReason
			Case DLL_PROCESS_ATTACH
				xyLib_Init(hinstDLL)
			Case DLL_PROCESS_DETACH
				xyLib_Unit()
			Case DLL_THREAD_ATTACH
				
			Case DLL_THREAD_DETACH
				
		End Select
		Return 1
	End Function
	
#EndIf
