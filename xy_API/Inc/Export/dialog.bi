


Extern "Windows-MS"
	
	Function Dlg_OpenFile(Parent As HWND, DefPath As ZString Ptr, Filter As ZString Ptr) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		AutoFreeBuffer.AllocMemory(MAX_PATH)
		AutoFreeBuffer.PutData(DefPath)
		Dim sl As UInteger = strlen(Filter)
		For i As Integer = 0 To sl - 1
			If Filter[i] = 124 Then Filter[i] = 0
		Next
		If xRtl.Dlg.OpenFile(Parent, AutoFreeBuffer.BufferMemory, MAX_PATH, Filter) Then
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Dlg_OpenFiles(Parent As HWND, DefPath As ZString Ptr, Filter As ZString Ptr) As ZString Ptr Export
		Dim pMemory As UByte Ptr = xRtl.TempMemory(65536)
		*Cast(ZString Ptr, pMemory) = *DefPath
		Dim sl As UInteger = strlen(Filter)
		For i As Integer = 0 To sl - 1
			If Filter[i] = 124 Then Filter[i] = 0
		Next
		If xRtl.Dlg.OpenFile(Parent, pMemory, 65536, Filter, , OFN_ALLOWMULTISELECT) Then
			Dim sDir As ZString * MAX_PATH
			Dim iDir As UInteger
			Dim pStr As ZString Ptr = pMemory
			AutoFreeBuffer.FreeMemory()
			For i As Integer = 0 To 65536
				If pMemory[i] = 0 Then
					If pMemory[i+1] = 0 Then
						AutoFreeBuffer.PutData(@sDir, iDir)
						AutoFreeBuffer.PutData(pStr)
						Exit For
					Else
						If iDir = 0 Then
							sDir = *Cast(ZString Ptr, pMemory)
							If pMemory[i-1] <> 92 Then sDir &= "\"
							iDir = strlen(@sDir)
							pStr = @pMemory[i+1]
						Else
							AutoFreeBuffer.PutData(@sDir, iDir)
							AutoFreeBuffer.PutData(pStr)
							AutoFreeBuffer.PutData(!"\r\n", 2)
							pStr = @pMemory[i+1]
						EndIf
					EndIf
				EndIf
			Next
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Dlg_SaveFile(Parent As HWND, DefPath As ZString Ptr, Filter As ZString Ptr) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		AutoFreeBuffer.AllocMemory(MAX_PATH)
		AutoFreeBuffer.PutData(DefPath)
		Dim sl As UInteger = strlen(Filter)
		For i As Integer = 0 To sl - 1
			If Filter[i] = 124 Then Filter[i] = 0
		Next
		If xRtl.Dlg.SaveFile(Parent, AutoFreeBuffer.BufferMemory, MAX_PATH, Filter) Then
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Dlg_SelFolder(Parent As HWND) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		AutoFreeBuffer.AllocMemory(MAX_PATH)
		If xRtl.Dlg.OpenFolder(Parent, AutoFreeBuffer.BufferMemory) Then
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Dlg_SelFont(Parent As HWND) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		Dim pFont As LOGFONT Ptr
		pFont = calloc(1, SizeOf(LOGFONT))
		If xRtl.Dlg.SelFont(Parent, pFont) Then
			AutoFreeBuffer.PutData(pFont->lfFaceName & "|")
			Dim hDW As HWND = GetDesktopWindow()
			Dim hDC As HDC = GetDC(hDW)
			AutoFreeBuffer.PutData(-MulDiv(pFont->lfHeight, 72, GetDeviceCaps(hDC, LOGPIXELSY)) & "|")
			ReleaseDC(hDW, hDC)
			AutoFreeBuffer.PutData(pFont->lfWeight & "|")
			AutoFreeBuffer.PutData(pFont->lfItalic & "|")
			AutoFreeBuffer.PutData(pFont->lfCharSet & "|")
			AutoFreeBuffer.PutData(pFont->lfUnderline & "|" & pFont->lfStrikeOut)
			free(pFont)
			Return AutoFreeBuffer.BufferMemory
		Else
			free(pFont)
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Dlg_SelColor(Parent As HWND, defColor As Integer) As Integer Export
		Dim tmpColor As Integer = defColor
		If xRtl.Dlg.SelColor(Parent, @tmpColor) Then
			Return tmpColor
		Else
			Return &HFF00FF
		EndIf
	End Function
	
End Extern
