


Extern "Windows-MS"
	
	Function Lock_Create() As Any Ptr Export
		Return MutexCreate()
	End Function
	
	Sub Lock_Free(pLock As Any Ptr) Export
		MutexDestroy(pLock)
	End Sub
	
	Sub Lock_On(pLock As Any Ptr) Export
		MutexLock(pLock)
	End Sub
	
	Sub Lock_Off(pLock As Any Ptr) Export
		MutexUnLock(pLock)
	End Sub
	
End Extern
