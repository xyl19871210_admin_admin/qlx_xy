


Extern "Windows-MS"
	
	Function A2W(ZStrPtr As ZString Ptr, ZStrLen As UInteger = 0) As WString Ptr Export
		Return xRtl.Charset.A2W(ZStrPtr, ZStrLen)
	End Function
	
	Function W2A(WStrPtr As WString Ptr, WStrLen As UInteger = 0) As ZString Ptr Export
		Return xRtl.Charset.W2A(WStrPtr, WStrLen)
	End Function
	
	Function W2U(WStrPtr As WString Ptr, WStrLen As UInteger = 0) As ZString Ptr Export
		Return xRtl.Charset.W2U(WStrPtr, WStrLen)
	End Function
	
	Function U2W(UTF8Ptr As ZString Ptr,UTF8Len As UInteger = 0) As WString Ptr Export
		Return xRtl.Charset.U2W(UTF8Ptr, UTF8Len)
	End Function
	
	Function A2U(ZStr As ZString Ptr, ZLen As UInteger = 0) As ZString Ptr Export
		Return xRtl.Charset.A2U(ZStr, ZLen)
	End Function
	
	Function U2A(UStr As ZString Ptr,ULen As UInteger = 0) As ZString Ptr Export
		Return xRtl.Charset.U2A(UStr, ULen)
	End Function
	
End Extern
