


Dim Shared NtWow64QueryInformationProcess64 As Function(ProcessHandle As HANDLE, ProcessInformationClass As UINT, ProcessInformation As PVOID, ProcessInformationLength As ULong, ReturnLength As PULONG) As Integer
Dim Shared NtQueryInformationProcess As Function(ProcessHandle As HANDLE, ProcessInformationClass As UINT, ProcessInformation As PVOID, ProcessInformationLength As ULong, ReturnLength As PULONG) As Integer
Dim Shared NtReadVirtualMemory As Function(ProcessHandle As HANDLE, BaseAddress As PVOID, Buffer As PVOID, NumberOfBytesToRead As SIZE_T, NumberOfBytesReaded As PULONG) As Integer
Dim Shared NtWow64ReadVirtualMemory64 As Function(ProcessHandle As HANDLE, BaseAddress As ULongInt, Buffer As PVOID, NumberOfBytesToRead As UINT64, NumberOfBytesReaded As PUINT64) As Integer
Dim Shared Proc_IsWow64Process As Function(pHdr As HANDLE, pBol As Integer Ptr) As Integer
Dim Shared ZwSuspendProcess As Function(hProcess As HANDLE) As Integer
Dim Shared ZwResumeProcess As Function(hProcess As HANDLE) As Integer
'NtQuerySystemInformation



Extern "Windows-MS"
	
	' 判断是否为64位进程
	Function Process_Is64(pHdr As HANDLE) As Integer Export
		If Global_Sys_Bits = 64 Then
			If Proc_IsWow64Process Then
				Dim Is64 As Integer
				Proc_IsWow64Process(pHdr, @Is64)
				Return IIf(Is64, 0, -1)
			EndIf
		EndIf
	End Function
	Function PID_Is64(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)
		If hProcess Then
			Function = Process_Is64(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 获取进程命令行
	Function Process_CmdLine(pHdr As HANDLE) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		If Process_Is64(pHdr) Then
			Dim pbi As PROCESS_BASIC_INFORMATION_64
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_64
			Dim pps As PROCESS_PARAMETERS_64
			If NT_SUCCESS(NtWow64QueryInformationProcess64(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), NULL)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.CommandLine.MaximumLength)
						If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pps.CommandLine.Buffer, sCmdLine, pps.CommandLine.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		Else
			Dim pbi As PROCESS_BASIC_INFORMATION_32
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_32
			Dim pps As PROCESS_PARAMETERS_32
			If NT_SUCCESS(NtQueryInformationProcess(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtReadVirtualMemory(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtReadVirtualMemory(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), 0)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.CommandLine.MaximumLength)
						If NT_SUCCESS(NtReadVirtualMemory(pHdr, pps.CommandLine.Buffer, sCmdLine, pps.CommandLine.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		EndIf
	End Function
	Function PID_CmdLine(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Process_CmdLine(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取进程 DLL加载路径 [用处不大]
	/'
	Function Process_DllPath(pHdr As HANDLE) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		If Process_Is64(pHdr) Then
			Dim pbi As PROCESS_BASIC_INFORMATION_64
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_64
			Dim pps As PROCESS_PARAMETERS_64
			If NT_SUCCESS(NtWow64QueryInformationProcess64(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), NULL)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.DllPath.MaximumLength)
						If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pps.DllPath.Buffer, sCmdLine, pps.DllPath.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		Else
			Dim pbi As PROCESS_BASIC_INFORMATION_32
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_32
			Dim pps As PROCESS_PARAMETERS_32
			If NT_SUCCESS(NtQueryInformationProcess(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtReadVirtualMemory(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtReadVirtualMemory(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), 0)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.DllPath.MaximumLength)
						If NT_SUCCESS(NtReadVirtualMemory(pHdr, pps.DllPath.Buffer, sCmdLine, pps.DllPath.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		EndIf
	End Function
	Function PID_DllPath(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Process_DllPath(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	'/
	
	' 获取进程工作目录
	Function Process_CurPath(pHdr As HANDLE) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		If Process_Is64(pHdr) Then
			Dim pbi As PROCESS_BASIC_INFORMATION_64
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_64
			Dim pps As PROCESS_PARAMETERS_64
			If NT_SUCCESS(NtWow64QueryInformationProcess64(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), NULL)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.CurrentDirectory.DosPath.MaximumLength)
						If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pps.CurrentDirectory.DosPath.Buffer, sCmdLine, pps.CurrentDirectory.DosPath.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		Else
			Dim pbi As PROCESS_BASIC_INFORMATION_32
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_32
			Dim pps As PROCESS_PARAMETERS_32
			If NT_SUCCESS(NtQueryInformationProcess(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtReadVirtualMemory(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtReadVirtualMemory(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), 0)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.CurrentDirectory.DosPath.MaximumLength)
						If NT_SUCCESS(NtReadVirtualMemory(pHdr, pps.CurrentDirectory.DosPath.Buffer, sCmdLine, pps.CurrentDirectory.DosPath.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		EndIf
	End Function
	Function PID_CurPath(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Process_CurPath(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取进程映像名
	Function Process_ImageName(pHdr As HANDLE) As ZString Ptr Export
		Dim sFile As ZString Ptr = xRtl.TempMemory(MAX_PATH)
		If GetModuleBaseName(pHdr, NULL, sFile, MAX_PATH) Then
			Return sFile
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	Function PID_ImageName(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Process_ImageName(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取进程文件路径
	Function Process_GetPath(pHdr As HANDLE) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		If Process_Is64(pHdr) Then
			Dim pbi As PROCESS_BASIC_INFORMATION_64
			Dim peb As PROCESS_ENVIRONMENT_BLOCK_64
			Dim pps As PROCESS_PARAMETERS_64
			If NT_SUCCESS(NtWow64QueryInformationProcess64(pHdr, ProcessBasicInformation, @pbi, sizeof(pbi), 0)) Then
				If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pbi.PebBaseAddress, @peb, SizeOf(peb), 0)) Then
					If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, peb.ProcessParameters, @pps, SizeOf(pps), NULL)) Then
						Dim sCmdLine As WString Ptr = malloc(pps.ImagePathName.MaximumLength)
						If NT_SUCCESS(NtWow64ReadVirtualMemory64(pHdr, pps.ImagePathName.Buffer, sCmdLine, pps.ImagePathName.MaximumLength, NULL)) Then
							Function = W2A(sCmdLine, 0)
						EndIf
						free(sCmdLine)
					EndIf
				EndIf
			EndIf
		Else
			Dim FilePath As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			GetModuleFileNameEx(pHdr, NULL, FilePath, MAX_PATH)
			Return FilePath
		EndIf
	End Function
	Function PID_GetPath(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Process_GetPath(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取指定进程使用的内存数量
	Function Proecss_WorkMenory(pHdr As HANDLE) As UInteger Export
		Dim pmc As PROCESS_MEMORY_COUNTERS
		pmc.cb = SizeOf(PROCESS_MEMORY_COUNTERS)
		If GetProcessMemoryInfo(pHdr, @pmc, SizeOf(PROCESS_MEMORY_COUNTERS)) Then
			Return pmc.WorkingSetSize
		EndIf
	End Function
	Function PID_WorkMenory(pid As Integer) As UInteger Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Proecss_WorkMenory(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 获取指定进程使用的虚拟内存数量
	Function Proecss_PageMenory(pHdr As HANDLE) As UInteger Export
		Dim pmc As PROCESS_MEMORY_COUNTERS
		pmc.cb = SizeOf(PROCESS_MEMORY_COUNTERS)
		If GetProcessMemoryInfo(pHdr, @pmc, SizeOf(PROCESS_MEMORY_COUNTERS)) Then
			Return pmc.PagefileUsage
		EndIf
	End Function
	Function PID_PageMenory(pid As Integer) As UInteger Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, FALSE, pid)
		If hProcess Then
			Function = Proecss_PageMenory(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 获取进程路径 [废弃代码]
	/'
	Function Process_GetPath(pHdr As HANDLE) As ZString Ptr Export
		If Process_Is64(pHdr) Then
			Dim FilePath As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			Dim NtPath As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			GetProcessImageFileName(pHdr, FilePath, MAX_PATH)
			' 转换 DosPath 为 NtPath
			Dim sDriveList As ZString * MAX_PATH
			Dim sDriveName As ZString * 64
			Dim As Integer i, iDriveSize
			If GetLogicalDriveStrings(MAX_PATH, sDriveList) Then
				Do Until sDriveList[i] = 0
					sDriveList[i+2] = 0
					If QueryDosDevice(@sDriveList[i], @sDriveName, 64) Then
						iDriveSize = strlen(sDriveName)
						If _strnicmp(FilePath, sDriveName, iDriveSize) = 0 Then
							strcpy(NtPath, @sDriveList[i])
							strcat(NtPath, FilePath + iDriveSize)
							Return NtPath
						EndIf
					EndIf
					i += 4
				Loop
			EndIf
			Return @xRtl.xywh_library_xrtl_memory_snull
		Else
			Dim FilePath As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			GetModuleFileNameEx(pHdr, NULL, FilePath, MAX_PATH)
			Return FilePath
		EndIf
	End Function
	Function PID_GetPath(pid As Integer) As ZString Ptr Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)
		If hProcess Then
			Function = Process_GetPath(hProcess)
			CloseHandle(hProcess)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	'/
	
	' 获取父进程PID
	Function Process_Parent(pHdr As HANDLE) As Integer Export
		If NtQueryInformationProcess Then
			Dim pbi As PROCESS_BASIC_INFORMATION_32
			If NT_SUCCESS(NtQueryInformationProcess(pHdr, ProcessBasicInformation, @pbi, SizeOf(pbi), 0)) Then
				Return pbi.InheritedFromUniqueProcessId
			EndIf
		EndIf
	End Function
	Function PID_Parent(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid)
		If hProcess Then
			Function = Process_Parent(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 获取进程PID
	Function Process_H2ID(pHdr As HANDLE) As Integer Export
		Return GetProcessId(pHdr)
	End Function
	
	' 遍历进程
	Function Process_List() As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		Dim lSnapShot As HANDLE = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)
		If lSnapShot <> -1 Then
			Dim tPE As PROCESSENTRY32
			tPE.dwSize = SizeOf(tPE)
			Dim lNextProcess As Integer = Process32First(lSnapShot, @tPE)
			Do While lNextProcess
				AutoFreeBuffer.PutData(tPE.szExeFile & "|" & tPE.th32ProcessID & "|" & tPE.pcPriClassBase & "|" & tPE.cntThreads & !"\r\n")
				lNextProcess = Process32Next(lSnapShot, @tPE)
			Loop
			CloseHandle(lSnapShot)
		EndIf
		If AutoFreeBuffer.BufferMemory Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 查找第一个子进程PID
	Function PID_FindChild(pid As Integer) As Integer Export
		Dim lSnapShot As HANDLE = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)
		If lSnapShot <> -1 Then
			Dim tPE As PROCESSENTRY32
			tPE.dwSize = SizeOf(tPE)
			Dim lNextProcess As Integer = Process32First(lSnapShot, @tPE)
			Do While lNextProcess
				If tPE.th32ParentProcessID = pid Then
					Function = tPE.th32ProcessID
					Exit Do
				EndIf
				lNextProcess = Process32Next(lSnapShot, @tPE)
			Loop
			CloseHandle(lSnapShot)
		EndIf
	End Function
	Function Process_FindChild(pHdr As HANDLE) As Integer Export
		Return PID_FindChild(GetProcessId(pHdr))
	End Function
	
	' 查找所有子进程
	Function PID_ListChild(pid As Integer) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		Dim lSnapShot As HANDLE = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)
		If lSnapShot <> -1 Then
			Dim tPE As PROCESSENTRY32
			tPE.dwSize = SizeOf(tPE)
			Dim lNextProcess As Integer = Process32First(lSnapShot, @tPE)
			Do While lNextProcess
				If tPE.th32ParentProcessID = pid Then
					AutoFreeBuffer.PutData(tPE.szExeFile & "|" & tPE.th32ProcessID & "|" & tPE.pcPriClassBase & "|" & tPE.cntThreads & !"\r\n")
				EndIf
				lNextProcess = Process32Next(lSnapShot, @tPE)
			Loop
			CloseHandle(lSnapShot)
		EndIf
		If AutoFreeBuffer.BufferMemory Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	Function Process_ListChild(pHdr As HANDLE) As ZString Ptr Export
		Return PID_ListChild(GetProcessId(pHdr))
	End Function
	
	' 进程状态
	Function Process_Status(pHdr As HANDLE) As Integer Export
		Dim ExitCode As Integer
		If GetExitCodeProcess(pHdr, @ExitCode) Then
			If ExitCode = STILL_ACTIVE Then
				Return -1
			EndIf
		EndIf
	End Function
	Function PID_Status(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid)
		If hProcess Then
			Function = Process_Status(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 结束进程
	Function Process_Kill(pHdr As HANDLE) As Integer Export
		If pHdr Then
			TerminateProcess(pHdr, 0)
			Return WaitForSingleObject(pHdr, INFINITE)
		EndIf
	End Function
	Function PID_Kill(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)
		If hProcess Then
			TerminateProcess(hProcess, 0)
			Function = WaitForSingleObject(hProcess, INFINITE)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 暂停运行
	Function Process_Pause(pHdr As HANDLE) As Integer Export
		Return ZwSuspendProcess(pHdr)
	End Function
	Function PID_Pause(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)
		If hProcess Then
			Function = ZwSuspendProcess(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 恢复运行
	Function Process_Resume(pHdr As HANDLE) As Integer Export
		Return ZwResumeProcess(pHdr)
	End Function
	Function PID_Resume(pid As Integer) As Integer Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)
		If hProcess Then
			Function = ZwResumeProcess(hProcess)
			CloseHandle(hProcess)
		EndIf
	End Function
	
	' 注入DLL
	Function Process_InjectDll(hProcess As HANDLE, sDll As ZString Ptr) As HANDLE Export
		Dim sLen As Integer = strlen(sDll)
		Dim Addr As Any Ptr = VirtualAllocEx(hProcess, NULL, sLen + 1, MEM_COMMIT Or MEM_RESERVE, PAGE_EXECUTE_READWRITE)
		Dim hThread As HANDLE
		If Addr Then
			If WriteProcessMemory(hProcess, Addr, sDll, sLen + 1, NULL) Then
				Dim LoadLibraryAddr As Any Ptr = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA")
				hThread = CreateRemoteThread(hProcess, NULL, 0, Cast( Any Ptr, LoadLibraryAddr), Addr, 0, NULL)
			EndIf
			WaitForSingleObject(hThread, INFINITE)
			Dim hDll As DWORD
			GetExitCodeThread(hThread, @hDll)
			CloseHandle(hThread)
			VirtualFreeEx(hProcess, Addr, sLen + 1, MEM_RELEASE)
			Return Cast(HANDLE, hDll)
		EndIf
	End Function
	Function PID_InjectDll(pid As Integer, sDll As ZString Ptr) As HANDLE Export
		Dim hProcess As HANDLE = OpenProcess(PROCESS_ALL_ACCESS Or PROCESS_CREATE_THREAD Or PROCESS_VM_OPERATION Or PROCESS_VM_WRITE, FALSE, pid)
		If hProcess Then
			Function = Process_InjectDll(hProcess, sDll)
			CloseHandle(hProcess)
		EndIf
	End Function
	
End Extern
