


Extern "Windows-MS"
	
	Sub Mouse_Move(hWin As HWND, x As Integer, y As Integer) Export
		If hWin Then
			SendMessage(hWin, WM_MOUSEMOVE, 0, MAKELONG(x, y))
		Else
			Dim TI As INPUT_
			TI.Type = INPUT_MOUSE
			TI.mi.dx = ((&HFFFF \ (GetSystemMetrics(SM_CXSCREEN) - 1)) * x)
			TI.mi.dy = ((&HFFFF \ (GetSystemMetrics(SM_CYSCREEN) - 1)) * y)
			TI.mi.dwFlags = MOUSEEVENTF_MOVE Or MOUSEEVENTF_ABSOLUTE
			TI.mi.time = GetTickCount()
			TI.mi.dwExtraInfo = GetMessageExtraInfo()
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
	Sub Mouse_Click(hWin As HWND, x As Integer, y As Integer, iBtn As Integer) Export
		If hWin Then
			Select Case iBtn
				Case 1
					SendMessage(hWin, WM_RBUTTONDOWN, 0, MAKELONG(x, y))
					SendMessage(hWin, WM_RBUTTONUP, 0, MAKELONG(x, y))
				Case 2
					SendMessage(hWin, WM_MBUTTONDOWN, 0, MAKELONG(x, y))
					SendMessage(hWin, WM_MBUTTONUP, 0, MAKELONG(x, y))
				Case Else
					SendMessage(hWin, WM_LBUTTONDOWN, 0, MAKELONG(x, y))
					SendMessage(hWin, WM_LBUTTONUP, 0, MAKELONG(x, y))
			End Select
		Else
			Mouse_Move(NULL, x, y)
			Dim TI(1) As INPUT_
			TI(0).Type = INPUT_MOUSE
			TI(1).Type = INPUT_MOUSE
			Select Case iBtn
				Case 1
					TI(0).mi.dwFlags = MOUSEEVENTF_RIGHTDOWN
					TI(1).mi.dwFlags = MOUSEEVENTF_RIGHTUP
				Case 2
					TI(0).mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN
					TI(1).mi.dwFlags = MOUSEEVENTF_MIDDLEUP
				Case Else
					TI(0).mi.dwFlags = MOUSEEVENTF_LEFTDOWN
					TI(1).mi.dwFlags = MOUSEEVENTF_LEFTUP
			End Select
			TI(0).mi.time = GetTickCount()
			TI(1).mi.time = GetTickCount()
			TI(0).mi.dwExtraInfo = GetMessageExtraInfo()
			TI(1).mi.dwExtraInfo = GetMessageExtraInfo()
			SendInput(2, @TI(0), SizeOf(TI))
		EndIf
	End Sub
	
	Sub Mouse_Down(hWin As HWND, x As Integer, y As Integer, iBtn As Integer) Export
		If hWin Then
			Select Case iBtn
				Case 1
					SendMessage(hWin, WM_RBUTTONDOWN, 0, MAKELONG(x, y))
				Case 2
					SendMessage(hWin, WM_MBUTTONDOWN, 0, MAKELONG(x, y))
				Case Else
					SendMessage(hWin, WM_LBUTTONDOWN, 0, MAKELONG(x, y))
			End Select
		Else
			Mouse_Move(NULL, x, y)
			Dim TI As INPUT_
			TI.Type = INPUT_MOUSE
			Select Case iBtn
				Case 1
					TI.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN
				Case 2
					TI.mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN
				Case Else
					TI.mi.dwFlags = MOUSEEVENTF_LEFTDOWN
			End Select
			TI.mi.time = GetTickCount()
			TI.mi.dwExtraInfo = GetMessageExtraInfo()
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
	Sub Mouse_Up(hWin As HWND, x As Integer, y As Integer, iBtn As Integer) Export
		If hWin Then
			Select Case iBtn
				Case 1
					SendMessage(hWin, WM_RBUTTONUP, 0, MAKELONG(x, y))
				Case 2
					SendMessage(hWin, WM_MBUTTONUP, 0, MAKELONG(x, y))
				Case Else
					SendMessage(hWin, WM_LBUTTONUP, 0, MAKELONG(x, y))
			End Select
		Else
			Mouse_Move(NULL, x, y)
			Dim TI As INPUT_
			TI.Type = INPUT_MOUSE
			Select Case iBtn
				Case 1
					TI.mi.dwFlags = MOUSEEVENTF_RIGHTUP
				Case 2
					TI.mi.dwFlags = MOUSEEVENTF_MIDDLEUP
				Case Else
					TI.mi.dwFlags = MOUSEEVENTF_LEFTUP
			End Select
			TI.mi.time = GetTickCount()
			TI.mi.dwExtraInfo = GetMessageExtraInfo()
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
	Sub Mouse_Whell(hWin As HWND, iVal As Integer) Export
		If hWin Then
			SendMessage(hWin, WM_MOUSEWHEEL, MAKELONG(0, iVal), MAKELONG(0, 0))
		Else
			Dim TI As INPUT_
			TI.Type = INPUT_MOUSE
			TI.mi.MouseData = iVal
			TI.mi.dwFlags = MOUSEEVENTF_WHEEL
			TI.mi.time = GetTickCount()
			TI.mi.dwExtraInfo = GetMessageExtraInfo()
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
	/'
	Function Mouse_GetPos() As Integer Export
		Dim TP As Point
		GetCursorPos(@TP)
		Return MAKELONG(TP.x, TP.y)
	End Function
	
	Sub Mouse_SetPos(x As Integer, y As Integer) Export
		SetCursorPos(x, y)
	End Sub
	'/
	
	Sub Mouse_Lock(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer) Export
		Dim TR As RECT
		TR.left = x1
		TR.top = y1
		TR.right = x2
		TR.bottom = y2
		ClipCursor(@TR)
	End Sub
	
	Sub Mouse_UnLock() Export
		ClipCursor(NULL)
	End Sub
	
End Extern
