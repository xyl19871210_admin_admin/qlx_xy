


Extern "Windows-MS"
	
	Function xCRC32 Alias "CRC32" (pDat As ZString Ptr, iSize As UInteger) As UInteger Export
		If iSize = 0 Then
			iSize = strlen(pDat)
		EndIf
		Return CRC32(pDat, iSize, INIT_CRC32)
	End Function
	
	Function CRC32_File(sPath As ZString Ptr) As UInteger Export
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				Function = CRC32(AutoFreeBuffer.BufferMemory, FileSize, INIT_CRC32)
				AutoFreeBuffer.FreeMemory()
			EndIf
			CloseHandle(FileHdr)
		EndIf
	End Function
	
	Function MD5(pDat As ZString Ptr, iSize As UInteger) As ZString Ptr Export
		If iSize = 0 Then
			iSize = strlen(pDat)
		EndIf
		Dim RetStr As ZString Ptr = xRtl.TempMemory((MD5_DIGESTSIZE * 2) + 2)
		MD5Init()
		MD5Update(pDat, iSize)
		HexEncoder(MD5Final(), MD5_DIGESTSIZE, RetStr)
		Return RetStr
	End Function
	
	Function MD5_File(sPath As ZString Ptr) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				Dim RetStr As ZString Ptr = xRtl.TempMemory((MD5_DIGESTSIZE * 2) + 2)
				MD5Init()
				MD5Update(AutoFreeBuffer.BufferMemory, FileSize)
				HexEncoder(MD5Final(), MD5_DIGESTSIZE, RetStr)
				Function = RetStr
				AutoFreeBuffer.FreeMemory()
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
			CloseHandle(FileHdr)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function SHA1(pDat As ZString Ptr, iSize As UInteger) As ZString Ptr Export
		If iSize = 0 Then
			iSize = strlen(pDat)
		EndIf
		Dim RetStr As ZString Ptr = xRtl.TempMemory((SHA1_DIGESTSIZE * 2) + 2)
		SHA1Init()
		SHA1Update(pDat, iSize)
		HexEncoder(SHA1Final(), SHA1_DIGESTSIZE, RetStr)
		Return RetStr
	End Function
	
	Function SHA1_File(sPath As ZString Ptr) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				Dim RetStr As ZString Ptr = xRtl.TempMemory((SHA1_DIGESTSIZE * 2) + 2)
				SHA1Init()
				SHA1Update(AutoFreeBuffer.BufferMemory, FileSize)
				HexEncoder(SHA1Final(), SHA1_DIGESTSIZE, RetStr)
				Function = RetStr
				AutoFreeBuffer.FreeMemory()
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
			CloseHandle(FileHdr)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function SHA256(pDat As ZString Ptr, iSize As UInteger) As ZString Ptr Export
		If iSize = 0 Then
			iSize = strlen(pDat)
		EndIf
		Dim RetStr As ZString Ptr = xRtl.TempMemory((SHA256_DIGESTSIZE * 2) + 2)
		SHA256Init()
		SHA256Update(pDat, iSize)
		HexEncoder(SHA256Final(), SHA256_DIGESTSIZE, RetStr)
		Return RetStr
	End Function
	
	Function SHA256_File(sPath As ZString Ptr) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				Dim RetStr As ZString Ptr = xRtl.TempMemory((SHA256_DIGESTSIZE * 2) + 2)
				SHA256Init()
				SHA256Update(AutoFreeBuffer.BufferMemory, FileSize)
				HexEncoder(SHA256Final(), SHA256_DIGESTSIZE, RetStr)
				Function = RetStr
				AutoFreeBuffer.FreeMemory()
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
			CloseHandle(FileHdr)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Base64_Encode(pDat As ZString Ptr, iSize As UInteger) As ZString Ptr Export
		If iSize = 0 Then
			iSize = strlen(pDat)
		EndIf
		AutoFreeBuffer.AllocMemory((iSize * 4 / 3) + 2)
		Base64Encode(pDat, iSize, AutoFreeBuffer.BufferMemory)
		Return AutoFreeBuffer.BufferMemory
	End Function
	
	Function Base64_Decode(pDat As ZString Ptr) As ZString Ptr Export
		AutoFreeBuffer.AllocMemory(strlen(pDat) + 2)
		AutoFreeBuffer.BufferMemory[Base64Decode(pDat, AutoFreeBuffer.BufferMemory)] = 0
		Return AutoFreeBuffer.BufferMemory
	End Function
	
	Function Base64_Encode_File(sPath As ZString Ptr) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				Dim RetStr As ZString Ptr = xRtl.TempMemory((FileSize * 4 / 3) + 2)
				Base64Encode(AutoFreeBuffer.BufferMemory, FileSize, RetStr)
				Function = RetStr
				AutoFreeBuffer.FreeMemory()
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
			CloseHandle(FileHdr)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Base64_Decode_File(sPath As ZString Ptr) As ZString Ptr Export
		Function = @xRtl.xywh_library_xrtl_memory_snull
		Dim FileHdr As HANDLE = xRtl.File.Open(sPath, -1)
		If FileHdr Then
			Dim FileSize As UInteger = xRtl.File.hSize(FileHdr)
			If FileSize Then
				AutoFreeBuffer.AllocMemory(FileSize + 1)
				xRtl.File.hRead(FileHdr, AutoFreeBuffer.BufferMemory, 0, FileSize)
				AutoFreeBuffer.BufferMemory[FileSize] = 0
				Dim RetStr As ZString Ptr = xRtl.TempMemory(FileSize + 2)
				RetStr[Base64Decode(AutoFreeBuffer.BufferMemory, RetStr)] = 0
				Function = RetStr
				AutoFreeBuffer.FreeMemory()
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
			CloseHandle(FileHdr)
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Url_Encode_GBK(sText As ZString Ptr, iSize As Integer) As ZString Ptr Export
		If iSize = 0 Then
			iSize = strlen(sText)
		EndIf
		Dim RetStr As ZString Ptr = xRtl.TempMemory(iSize * 3 + 1)
		*RetStr = ""
		For i As Integer = 0 To iSize - 1
			*RetStr &= "%" & Hex(Cast(UByte Ptr, sText)[i])
		Next
		Return RetStr
	End Function
	
End Extern
