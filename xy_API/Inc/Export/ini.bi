


Extern "Windows-MS"
	
	Function Ini_Read(file As ZString Ptr, sec As ZString Ptr, key As ZString Ptr) As ZString Ptr Export
		Return xRtl.Ini.GetStr(file, sec, key)
	End Function
	
	Function Ini_ReadInt(file As ZString Ptr, sec As ZString Ptr, key As ZString Ptr) As Integer Export
		Return xRtl.Ini.GetInt(file, sec, key)
	End Function
	
	Function Ini_Write(file As ZString Ptr, sec As ZString Ptr, key As ZString Ptr, value As ZString Ptr) As Integer Export
		Return xRtl.Ini.SetStr(file, sec, key, value)
	End Function
	
	Function Ini_EnumSec(file As ZString Ptr) As ZString Ptr Export
		Dim OutArr As ZString Ptr Ptr
		AutoFreeBuffer.FreeMemory()
		For i As Integer = 0 To xRtl.ini.EnumSec(file, @OutArr) - 1
			AutoFreeBuffer.PutData(OutArr[i], 0)
			AutoFreeBuffer.PutData(!"\r\n", 2)
		Next
		If AutoFreeBuffer.BufferMemory Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 1] = 0
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Ini_EnumKey(file As ZString Ptr, sec As ZString Ptr) As ZString Ptr Export
		Dim OutArr As ZString Ptr Ptr
		AutoFreeBuffer.FreeMemory()
		For i As Integer = 0 To xRtl.ini.EnumKey(file, sec, @OutArr) - 1
			AutoFreeBuffer.PutData(OutArr[i], 0)
			AutoFreeBuffer.PutData(!"\r\n", 2)
		Next
		If AutoFreeBuffer.BufferMemory Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 1] = 0
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	Function Ini_DelSec(file As ZString Ptr, sec As ZString Ptr) As Integer Export
		Return WritePrivateProfileString(sec, NULL, NULL, file)
	End Function
	
	Function Ini_DelKey(file As ZString Ptr, sec As ZString Ptr, key As ZString Ptr) As Integer Export
		Return WritePrivateProfileString(sec, key, NULL, file)
	End Function
	
End Extern
