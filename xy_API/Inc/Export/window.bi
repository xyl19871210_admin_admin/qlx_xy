


Type tz_EidStu
	Count As Integer
	Eid As Integer
	hWnd As HWND
	ClassName As ZString Ptr
End Type

Type tz_FindStu
	Mask As Integer
	ClassName As ZString Ptr
	Caption As ZString Ptr
	Style As Integer
	ExStyle As Integer
	PID As HANDLE
	Thread As HANDLE
	Count As Integer
	RetStr As ZString Ptr
	bk As Function(hWnd As HWND,Param As Integer) As Integer
	Param As Integer
End Type

#Define TZ_USEPID &H1				' 在指定进程PID下查找窗口

#Define TZ_USETHREAD &H2			' 在指定线程下查找窗口

#Define TZ_SKIPCLASS &H0			' 不检查窗口类名
#Define TZ_FULLCLASS &H4			' 窗口类名必须完全匹配
#Define TZ_PARTCLASS &H8			' 窗口类名部分匹配即可
#Define TZ_ELSECLASS &HC			' 窗口类名必须不匹配

#Define TZ_SKIPTITLE &H0			' 不检查窗口标题
#Define TZ_FULLTITLE &H10			' 窗口标题必须完全匹配
#Define TZ_PARTTITLE &H20			' 窗口标题部分匹配即可
#Define TZ_ELSETITLE &H30			' 窗口标题必须不匹配

#Define TZ_SKIPSTYLE &H0			' 不检查窗口风格
#Define TZ_EXISSTYLE &H40			' 窗口必须存在指定风格
#Define TZ_NONESTYLE &H80			' 窗口必须不存在指定风格
#Define TZ_MATESTYLE &HC0			' 窗口风格必须完全一致

#Define TZ_SKIPEXSTY &H0			' 不检查窗口扩展风格
#Define TZ_EXISEXSTY &H100			' 窗口必须存在指定扩展风格
#Define TZ_NONEEXSTY &H200			' 窗口必须不存在指定扩展风格
#Define TZ_MATEEXSTY &H300			' 窗口扩展风格必须完全一致



Function xGetCaption(hWnd As HWND) As ZString Ptr
	Dim iRet As Integer
	If SendMessageTimeout(hWnd, WM_GETTEXTLENGTH, 0, 0, SMTO_ABORTIFHUNG, 60, @iRet) Then
		If iRet > 0 Then
			Dim pMem As ZString Ptr = xRtl.TempMemory(iRet + 1)
			*pMem = ""
			SendMessageTimeout(hWnd, WM_GETTEXT, iRet + 1, Cast(Integer, pMem), SMTO_ABORTIFHUNG, 60, @iRet)
			pMem[iRet] = 0
			Return pMem
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	Else
		Return @xRtl.xywh_library_xrtl_memory_snull
	EndIf
End Function

Function xGetClassName(hWnd As HWND) As ZString Ptr
	Dim xywh_library_auto_memory As ZString Ptr = xRtl.TempMemory(MAX_PATH)
	GetClassName(hWnd, xywh_library_auto_memory, MAX_PATH)
	Return xywh_library_auto_memory
End Function

Function EidToHwnd_CheckClass(hWin As HWND, ClassName As ZString Ptr) As Integer
	If *ClassName = "" Then
		Return -1
	Else
		Dim TempStr As ZString * MAX_PATH
		GetClassName(hWin, TempStr, MAX_PATH)
		If InStr(TempStr, *ClassName) Then
			Return -1
		EndIf
	EndIf
End Function

Function EidToHwnd_Proc(hWin As HWND, lParam As tz_EidStu Ptr) As Integer
	If EidToHwnd_CheckClass(hWin, lParam->ClassName) Then
		lParam->Count += 1
		If lParam->Count = lParam->Eid Then
			lParam->hWnd = hWin
			Return 0
		EndIf
	EndIf
	Return TRUE
End Function

Function HwndToEid_Proc(hWin As HWND, lParam As tz_EidStu Ptr) As Integer
	If EidToHwnd_CheckClass(hWin, lParam->ClassName) Then
		lParam->Count += 1
		If hWin = lParam->hWnd Then
			Return 0
		EndIf
	EndIf
	Return TRUE
End Function

Function IsWindowOK(hWin As HWND, lParam As tz_FindStu Ptr) As Integer
	' 过滤风格
	Select Case lParam->Mask And TZ_MATESTYLE
		Case TZ_EXISSTYLE			' 存在风格
			If (GetWindowLong(hWin, GWL_STYLE) And lParam->Style) <> lParam->Style Then
				Return 0
			EndIf
		Case TZ_NONESTYLE			' 不存在风格
			If (GetWindowLong(hWin, GWL_STYLE) And lParam->Style) = lParam->Style Then
				Return 0
			EndIf
		Case TZ_MATESTYLE			' 完全匹配
			If GetWindowLong(hWin, GWL_STYLE) <> lParam->Style Then
				Return 0
			EndIf
	End Select
	' 过滤扩展风格
	Select Case lParam->Mask And TZ_MATEEXSTY
		Case TZ_EXISEXSTY			' 存在风格
			If (GetWindowLong(hWin, GWL_EXSTYLE) And lParam->ExStyle) <> lParam->ExStyle Then
				Return 0
			EndIf
		Case TZ_NONEEXSTY			' 不存在风格
			If (GetWindowLong(hWin, GWL_EXSTYLE) And lParam->ExStyle) = lParam->ExStyle Then
				Return 0
			EndIf
		Case TZ_MATEEXSTY			' 完全匹配
			If GetWindowLong(hWin, GWL_EXSTYLE) <> lParam->ExStyle Then
				Return 0
			EndIf
	End Select
	' 过滤 PID
	If lParam->Mask And TZ_USEPID Then
		Dim TempPID As Integer
		GetWindowThreadProcessId(hWin, @TempPID)
		If lParam->PID <> TempPID Then
			Return 0
		EndIf
	EndIf
	' 过滤线程
	If lParam->Mask And TZ_USETHREAD Then
		Dim TempPID As Integer = GetWindowThreadProcessId(hWin, NULL)
		If lParam->Thread <> TempPID Then
			Return 0
		EndIf
	EndIf
	' 过滤类名
	Select Case lParam->Mask And TZ_ELSECLASS
		Case TZ_FULLCLASS			' 完全匹配
			If StrCmp(xGetClassName(hWin), lParam->ClassName) Then
			'If *xGetClassName(hWin) <> *lParam->ClassName Then
				Return 0
			EndIf
		Case TZ_PARTCLASS			' 模糊匹配
			If *lParam->ClassName <> "" Then
				If InStr(*xGetClassName(hWin), *lParam->ClassName) <= 0 Then
					Return 0
				EndIf
			EndIf
		Case TZ_ELSECLASS			' 除此之外
			If StrCmp(xGetClassName(hWin), lParam->ClassName) = 0 Then
			'If *xGetClassName(hWin) = *lParam->ClassName Then
				Return 0
			EndIf
	End Select
	' 过滤标题
	Select Case lParam->Mask And TZ_ELSETITLE
		Case TZ_FULLTITLE			' 完全匹配
			If StrCmp(xGetCaption(hWin), lParam->Caption) Then
			'If *xGetCaption(hWin) <> *lParam->Caption Then
				Return 0
			EndIf
		Case TZ_PARTTITLE			' 模糊匹配
			If *lParam->Caption <> "" Then
				If InStr(*xGetCaption(hWin), *lParam->Caption) <= 0 Then
					Return 0
				EndIf
			EndIf
		Case TZ_ELSETITLE			' 除此之外
			If StrCmp(xGetCaption(hWin), lParam->Caption) = 0 Then
			'If *xGetCaption(hWin) = *lParam->Caption Then
				Return 0
			EndIf
	End Select
	Return -1
End Function

Function ScanWindow_Proc(hWin As HWND, lParam As tz_FindStu Ptr) As Integer
	If IsWindowOK(hWin, lParam) Then
		lParam->Count += 1
		If lParam->bk Then
			Return lParam->bk(hWin, lParam->Param)
		EndIf
	EndIf
	Return TRUE
End Function

Function ListWindow_Proc(hWin As HWND, lParam As tz_FindStu Ptr) As Integer
	If IsWindowOK(hWin, lParam) Then
		lParam->Count += 1
		AutoFreeBuffer.PutData(hWin & !"\r\n")
	EndIf
	Return TRUE
End Function

Function FindWindow_Proc(hWin As HWND, lParam As tz_FindStu Ptr) As Integer
	If IsWindowOK(hWin, lParam) Then
		lParam->Count = Cast(Integer, hWin)
		Return FALSE
	EndIf
	Return TRUE
End Function



Extern "Windows-MS"
	
	' 获取窗口标题
	Function Window_GetCaption(hWnd As HWND) As ZString Ptr Export
		Return xGetCaption(hWnd)
	End Function
	
	' 获取窗口类名
	Function Window_GetClassName(hWnd As HWND) As ZString Ptr Export
		Return xGetClassName(hWnd)
	End Function
	
	' 检测窗口是否挂起
	Function Window_IsHung(hWnd As HWND) As Integer Export
		Dim iRet As Integer
		If SendMessageTimeout(hWnd, WM_NULL, 0, 0, SMTO_ABORTIFHUNG, 1000, @iRet) = 0 Then
			Return -1
		EndIf
		Return 0
	End Function
	
	' 获取指定窗口的根窗口
	Function Window_GetRoot(hWnd As HWND) As HWND Export
		Dim TempHdr As HANDLE
		Do
			TempHdr = hWnd
			hWnd = GetParent(hWnd)
		Loop Until hWnd = NULL
		Return TempHdr
	End Function
	
	' 根据枚举值获取句柄
	Function Window_E2H(Parent As HWND, ClassName As ZString Ptr, Eid As Integer) As HWND Export
		Dim tes As tz_EidStu
		tes.Count = 0
		tes.Eid = Eid
		tes.ClassName = ClassName
		EnumChildWindows(Parent,Cast(Any Ptr,@EidToHwnd_Proc),Cast(Integer,@tes))
		Return tes.hWnd
	End Function
	
	' 根据句柄获取枚举值
	Function Window_H2E(Parent As HWND, ClassName As ZString Ptr, hWnd As HWND) As Integer Export
		Dim tes As tz_EidStu
		tes.Count = 0
		tes.hWnd = hWnd
		tes.ClassName = ClassName
		EnumChildWindows(Parent,Cast(Any Ptr,@HwndToEid_Proc),Cast(Integer,@tes))
		Return tes.Count
	End Function
	
	' 根据条件扫描窗口
	Function Window_Scan(Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE, bk As Any Ptr, Param As Integer) As Integer Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		tfs.Count = 0
		tfs.bk = bk
		tfs.Param = Param
		EnumWindows(Cast(Any Ptr, @ScanWindow_Proc), Cast(Integer, @tfs))
		Return tfs.Count
	End Function
	
	' 根据条件列出窗口
	Function Window_List(Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE) As ZString Ptr Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		tfs.Count = 0
		AutoFreeBuffer.FreeMemory()
		EnumWindows(Cast(Any Ptr, @ListWindow_Proc), Cast(Integer, @tfs))
		If tfs.Count > 0 Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 1] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 根据条件查找窗口
	Function Window_Find(Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE) As HWND Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		EnumWindows(Cast(Any Ptr, @FindWindow_Proc), Cast(Integer, @tfs))
		Return Cast(HWND,tfs.Count)
	End Function
	
	' 根据条件扫描子窗口
	Function Window_ScanChild(Parent As HWND, Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE, bk As Any Ptr, Param As Integer) As Integer Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		tfs.Count = 0
		tfs.bk = bk
		tfs.Param = Param
		EnumChildWindows(Parent, Cast(Any Ptr, @ScanWindow_Proc), Cast(Integer, @tfs))
		Return tfs.Count
	End Function
	
	' 根据条件列出子窗口
	Function Window_ListChild(Parent As HWND, Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE) As ZString Ptr Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		tfs.Count = 0
		AutoFreeBuffer.FreeMemory()
		EnumChildWindows(Parent, Cast(Any Ptr, @ListWindow_Proc), Cast(Integer, @tfs))
		If tfs.Count > 0 Then
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
			AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 1] = 0
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 根据条件查找子窗口
	Function Window_FindChild(Parent As HWND, Mask As Integer, ClassName As ZString Ptr, Caption As ZString Ptr, Style As Integer, ExStyle As Integer, PID As HANDLE, Thread As HANDLE) As HWND Export
		Dim tfs As tz_FindStu
		tfs.Mask = Mask
		tfs.ClassName = ClassName
		tfs.Caption = Caption
		tfs.Style = Style
		tfs.ExStyle = ExStyle
		tfs.PID = PID
		tfs.Thread = Thread
		EnumChildWindows(Parent,Cast(Any Ptr,@FindWindow_Proc),Cast(Integer,@tfs))
		Return Cast(HWND,tfs.Count)
	End Function
	
	' 获取窗口的进程PID
	Function Window_GetPID(hWin As HWND) As Integer Export
		Dim RetPID As Integer
		GetWindowThreadProcessId(hWin, @RetPID)
		Return RetPID
	End Function
	
	' 获取窗口的进程TID
	Function Window_GetTID(hWin As HWND) As Integer Export
		Dim RetPID As Integer
		Return GetWindowThreadProcessId(hWin, @RetPID)
	End Function
	
	' 获取窗口对应的程序路径
	Function Window_GetPath(hWin As HWND) As ZString Ptr Export
		Dim RetPID As Integer
		GetWindowThreadProcessId(hWin, @RetPID)
		Return PID_GetPath(RetPID)
	End Function
	
	' 获取指定位置的窗口
	Function Window_FormPoint(x As Integer, y As Integer) As HANDLE Export
		Dim pt As Point = (x, y)
		Return WindowFromPoint(pt)
	End Function
	
	' 获取指定位置的窗口
	Function Window_FormMouse() As HANDLE Export
		Dim pt As Point
		GetCursorPos(@pt)
		Return WindowFromPoint(pt)
	End Function
	
	' 屏幕坐标到窗口坐标
	Function Window_ConvS2C(hWin As HANDLE, x As Integer, y As Integer) As Integer Export
		Dim pt As Point = (x, y)
		If ScreenToClient(hWin, @pt) Then
			Return MAKELONG(pt.y, pt.x)
		EndIf
	End Function
	
	' 窗口坐标到屏幕坐标
	Function Window_ConvC2S(hWin As HANDLE, x As Integer, y As Integer) As Integer Export
		Dim pt As Point = (x, y)
		If ClientToScreen(hWin, @pt) Then
			Return MAKELONG(pt.y, pt.x)
		EndIf
	End Function
	
	' 获取窗口矩形
	Function Window_GetRect(hWin As HANDLE) As RECT Ptr Export
		Dim pRect As RECT Ptr = xRtl.TempMemory(SizeOf(RECT))
		If GetWindowRect(hWin, pRect) Then
			Return pRect
		EndIf
	End Function
	
End Extern
