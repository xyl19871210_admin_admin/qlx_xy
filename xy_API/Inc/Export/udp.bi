


Type xUDP
	h_Socket As HANDLE
	Event_Proc As HANDLE
	Event_Recv As HANDLE
End Type



' ���ûص�
Sub xSock_UDP_Proc(Hdr As Integer, Event As Integer, lpOverlapped As Iocp_Link, Size As Integer, Custom As xUDP Ptr)
	Select Case Event
		Case IOCP_UDP_RECV
			If Custom->Event_Proc Then
				Dim RecvPack As ZString Ptr
				Dim RecvIP As ZString Ptr
				Dim RecvPort As UShort
				RecvPack = UDP_Read(lpOverlapped, RecvIP, RecvPort)
				If RecvPack Then
					If Custom->Event_Recv Then
						AutoFreeBuffer.FreeMemory()
						AutoFreeBuffer.PutData(RecvPack, Size)
						SendMessage(Custom->Event_Recv, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					EndIf
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(!"Recv\r\n", 6)
					AutoFreeBuffer.PutData(RecvIP)
					AutoFreeBuffer.PutData(":", 1)
					AutoFreeBuffer.PutData(Str(RecvPort))
					SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case IOCP_UDP_SEND
			If Custom->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData("Send_Succ", 9)
				SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case IOCP_UDP_ESEND
			If Custom->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData("Send_Fail", 9)
				SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
	End Select
End Sub



Extern "Windows-MS"
	
	Function xUDP_Create Alias "UDP_Create" (ip As ZString Ptr, Port As Integer, EventProc As HANDLE, EventRecv As HANDLE) As Any Ptr Export
		Dim ClassPtr As xUDP Ptr = calloc(1, SizeOf(xUDP))
		If ClassPtr Then
			ClassPtr->Event_Proc = EventProc
			ClassPtr->Event_Recv = EventRecv
			ClassPtr->h_Socket = UDP_Create(ip, Port, NULL, @xSock_UDP_Proc, 1,Cast(Integer, ClassPtr))
			If ClassPtr->h_Socket Then
				Return ClassPtr
			Else
				free(ClassPtr)
				Return NULL
			EndIf
		EndIf
	End Function
	
	Sub xUDP_Destroy Alias "UDP_Destroy" (hSock As xUDP Ptr) Export
		If hSock Then
			UDP_Destroy(hSock->h_Socket)
			free(hSock)
		EndIf
	End Sub
	
	Function UDP_Send(hSock As xUDP Ptr, sPack As ZString Ptr, iSize As UInteger, ip As ZString Ptr, Port As Integer, Sync As Integer) As Integer Export
		If hSock Then
			If iSize = 0 Then
				iSize = strlen(sPack)
			EndIf
			Return UDP_Write(hSock->h_Socket, sPack, iSize, ip, Port, Sync)
		EndIf
	End Function
	
End Extern
