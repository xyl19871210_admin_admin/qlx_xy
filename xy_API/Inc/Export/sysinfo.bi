


Function xSys_Bits() As Integer
	GetNativeSystemInfo(@Global_Sys_Info)
	Select Case Global_Sys_Info.wProcessorArchitecture
		Case PROCESSOR_ARCHITECTURE_AMD64, PROCESSOR_ARCHITECTURE_IA64
			Return 64
		Case Else
			Return 32
	End Select
End Function



Extern "Windows-MS"
	
	' 获取系统位数
	Function Sys_Bits() As Integer Export
		Return Global_Sys_Bits
	End Function
	
End Extern
