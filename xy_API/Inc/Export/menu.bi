


Extern "Windows-MS"
	
	' 创建
	Function Menu_Create(Event As HANDLE) As HANDLE Export
		Dim ClassPtr As Menu_Data Ptr = calloc(1, SizeOf(Menu_Data))
		If ClassPtr Then
			ClassPtr->EventHandle = Event
			ClassPtr->MenuHandle = CreatePopupMenu()
			Return CreateDialogParam(hInst, Cast(ZString Ptr, 1170), Event, @MenuProc, Cast(Integer, ClassPtr))
		EndIf
	End Function
	
	' 销毁
	Sub Menu_Destroy(hMenu As HANDLE) Export
		SendMessage(hMenu, WM_CLOSE, 0, 0)
	End Sub
	
	' 添加
	Function Menu_Append(hMenu As HANDLE, sTitle As ZString Ptr, ID As Integer) As Integer Export
		Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hMenu, DWL_USER))
		If ClassPtr Then
			If *sTitle = "-" Then
				Return AppendMenu(ClassPtr->MenuHandle, MF_SEPARATOR, ID, NULL)
			Else
				Return AppendMenu(ClassPtr->MenuHandle, MF_STRING, ID, sTitle)
			EndIf
		EndIf
	End Function
	
	' 删除
	Function Menu_Delete(hMenu As HANDLE, ID As Integer) As Integer Export
		Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hMenu, DWL_USER))
		If ClassPtr Then
			Return DeleteMenu(ClassPtr->MenuHandle, ID, MF_BYCOMMAND)
		EndIf
	End Function
	
	' 弹出
	Function Menu_Popup(hMenu As HANDLE, x As Integer, y As Integer) As Integer Export
		Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hMenu, DWL_USER))
		If ClassPtr Then
			Return TrackPopupMenu(ClassPtr->MenuHandle, 0, x, y, 0, ClassPtr->WinHandle, NULL)
		EndIf
	End Function
	
	' 弹出 [在光标位置]
	Function Menu_PopupCursor(hMenu As HANDLE) As Integer Export
		Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hMenu, DWL_USER))
		If ClassPtr Then
			Dim curPos As POINT
			GetCursorPos(@curPos)
			Return TrackPopupMenu(ClassPtr->MenuHandle, 0, curPos.x, curPos.y, 0, ClassPtr->WinHandle, NULL)
		EndIf
	End Function
	
End Extern
