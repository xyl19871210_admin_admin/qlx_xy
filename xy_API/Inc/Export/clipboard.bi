


Extern "Windows-MS"
	
	Function Clip_GetText() As ZString Ptr Export
		Return xRtl.Clip.GetText()
	End Function
	
	Function Clip_SetText(sText As ZString Ptr, uSize As UInteger) As Integer Export
		Return xRtl.Clip.SetText(sText, uSize)
	End Function
	
End Extern
