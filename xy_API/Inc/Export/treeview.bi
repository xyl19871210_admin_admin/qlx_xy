


Extern "Windows-MS"
	
	' 创建树列表
	Function TreeView_Create(hParent As HANDLE, x As Integer, y As Integer, w As Integer, h As Integer, hEvent As HANDLE) As HANDLE Export
		Dim ClassPtr As TreeViewCon_Data Ptr = calloc(1, SizeOf(TreeViewCon_Data))
		If ClassPtr Then
			ClassPtr->Event = hEvent
			Dim RetHdr As HANDLE = CreateDialogParam(hInst, Cast(ZString Ptr, 1160), hParent, @TreeViewProc, Cast(Integer, ClassPtr))
			SetWindowPos(RetHdr, 0, x, y, w, h, SWP_NOZORDER Or SWP_NOACTIVATE)
			Return ClassPtr->ConHandle
		EndIf
	End Function
	
	' 添加节点
	Function TreeView_AddNode(hTree As HWND, sText As ZString Ptr, hParent As HTREEITEM) As HTREEITEM Export
		Dim tNode As TV_INSERTSTRUCT
		tNode.hParent = hParent
		tNode.hInsertAfter = TVI_LAST
		tNode.itemex.mask = TVIF_TEXT
		tNode.itemex.pszText = sText
		Return TreeView_InsertItem(hTree, @tNode)
	End Function
	
	' 删除节点
	Function TreeView_DelNode(hTree As HWND, hNode As HTREEITEM) As Integer Export
		Return TreeView_DeleteItem(hTree, hNode)
	End Function
	
	' 清空节点
	Function TreeView_Clear(hTree As HWND) As Integer Export
		Return TreeView_DeleteAllItems(hTree)
	End Function
	
	' 获取根节点
	Function TreeView_RootNode(hTree As HWND) As HTREEITEM Export
		Return TreeView_GetRoot(hTree)
	End Function
	
	' 获取项数量
	Function TreeView_Count(hTree As HWND) As Integer Export
		Return TreeView_GetCount(hTree)
	End Function
	
	' 读取文本
	Function TreeView_GetText(hTree As HWND, hNode As HTREEITEM) As ZString Ptr Export
		' 打开进程
		Dim PID As UInteger
		GetWindowThreadProcessId(hTree, @PID)
		Dim hProc As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID)
		If hProc Then
			' 申请内存
			Dim pItem As TV_ITEM Ptr = VirtualAllocEx(hProc, NULL, SizeOf(TV_ITEM), MEM_COMMIT, PAGE_READWRITE)
			Dim pText As ZString Ptr = VirtualAllocEx(hProc, NULL, MAX_PATH, MEM_COMMIT, PAGE_READWRITE)
			Dim sBuff As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			' 填充 TreeNode 结构
			Dim TreeNode As TV_ITEM
			TreeNode.mask = TVIF_TEXT
			TreeNode.hItem = hNode
			TreeNode.pszText = pText
			TreeNode.cchTextMax = MAX_PATH
			' 获取数据	
			WriteProcessMemory(hProc, pItem, @TreeNode, SizeOf(TV_ITEM), NULL)
			TreeView_GetItem(hTree, pItem)
			ReadProcessMemory(hProc, pText, sBuff, MAX_PATH, NULL)
			VirtualFreeEx(hProc, pItem, 0, MEM_RELEASE)
			VirtualFreeEx(hProc, pText, 0, MEM_RELEASE)
			Return sBuff
		EndIf
	End Function
	
	' 获取绝对列表的下一项
	Function TreeView_GetNextList(hTree As HWND, hNode As HTREEITEM) As HTREEITEM Export
		Dim hRet As HTREEITEM = TreeView_GetNextItem(hTree, hNode, TVGN_CHILD)
		If hRet = NULL Then
			hRet = TreeView_GetNextItem(hTree, hNode, TVGN_NEXT)
		EndIf
		If hRet = NULL Then
			Dim hParentItem As HTREEITEM = TreeView_GetNextItem(hTree, hNode, TVGN_PARENT)
			hRet = TreeView_GetNextItem(hTree, hParentItem, TVGN_NEXT)
		EndIf
		Return hRet
	End Function
	
	' 获取同级别的下一项
	Function TreeView_GetNextNode(hTree As HWND, hNode As HTREEITEM) As HTREEITEM Export
		Return TreeView_GetNextItem(hTree, hNode, TVGN_NEXT)
	End Function
	
	' 获取子项
	Function TreeView_GetChileNode(hTree As HWND, hNode As HTREEITEM) As HTREEITEM Export
		Return TreeView_GetNextItem(hTree, hNode, TVGN_CHILD)
	End Function
	
	' 获取选择项
	Function TreeView_GetSelNode(hTree As HWND) As HTREEITEM Export
		Return TreeView_GetSelection(hTree)
	End Function
	
	' 获取项坐标
	Function TreeView_GetNodeRect(hTree As HWND, hNode As HTREEITEM) As RECT Ptr Export
		' 打开进程
		Dim PID As UInteger
		GetWindowThreadProcessId(hTree, @PID)
		Dim hProc As HANDLE = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID)
		If hProc Then
			' 申请内存
			Dim pDstRect As RECT Ptr = VirtualAllocEx(hProc, NULL, SizeOf(RECT), MEM_COMMIT, PAGE_READWRITE)
			Dim pSrcRect As RECT Ptr = xRtl.TempMemory(SizeOf(RECT))
			' 选择 Node
			SendMessage(hTree, TVM_SELECTITEM, TVGN_CARET, Cast(Integer, hNode))
			TreeView_EnsureVisible(hTree, hNode)
			' 获取范围
			*Cast(HTREEITEM Ptr, pSrcRect) = hNode
			WriteProcessMemory(hProc, pDstRect, pSrcRect, SizeOf(RECT), NULL)
			SendMessage(hTree, TVM_GETITEMRECT, TRUE, Cast(Integer, pDstRect))
			ReadProcessMemory(hProc, pDstRect, pSrcRect, SizeOf(RECT), NULL)
			VirtualFreeEx(hProc, pDstRect, 0, MEM_RELEASE)
			Return pSrcRect
		EndIf
	End Function
	
	' 改变选择项
	Function TreeView_SelNode(hTree As HWND, hNode As HTREEITEM) As Integer Export
		Return SendMessage(hTree, TVM_SELECTITEM, TVGN_CARET, Cast(Integer, hNode))
	End Function
	
	' 模拟点击项
	Function TreeView_ClickNode(hTree As HWND, hNode As HTREEITEM) As Integer Export
		Dim pRect As RECT Ptr = TreeView_GetNodeRect(hTree, hNode)
		If pRect Then
			Dim x As UShort = pRect->Left + ((pRect->Right - pRect->Left) / 2)
			Dim y As UShort = pRect->top + ((pRect->bottom - pRect->top) / 2)
			SendMessage(hTree, WM_LBUTTONDOWN, 0, MAKELPARAM(x, y))
			SendMessage(hTree, WM_LBUTTONUP, 0, MAKELPARAM(x, y))
			Return -1
		EndIf
	End Function
	
	' 模拟双击项
	Function TreeView_DblClickNode(hTree As HWND, hNode As HTREEITEM) As Integer Export
		Dim pRect As RECT Ptr = TreeView_GetNodeRect(hTree, hNode)
		If pRect Then
			Dim x As UShort = pRect->Left + ((pRect->Right - pRect->Left) / 2)
			Dim y As UShort = pRect->top + ((pRect->bottom - pRect->top) / 2)
			SendMessage(hTree, WM_LBUTTONDBLCLK, 0, MAKELPARAM(x, y))
			Return -1
		EndIf
	End Function
	
	
	
	
End Extern
