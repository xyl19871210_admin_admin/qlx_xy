


Type RunApp_Info
	ReadPipe As HANDLE
	WritePipe As HANDLE
	sa As SECURITY_ATTRIBUTES
End Type



Extern "Windows-MS"
	
	Function RunApp_Console(sPath As ZString Ptr, iShow As Integer) As ZString Ptr
		Dim si As STARTUPINFO
		Dim pi As PROCESS_INFORMATION
		Dim ri As RunApp_Info
		' 创建管道
		ri.sa.nLength = SizeOf(SECURITY_ATTRIBUTES)
		ri.sa.lpSecurityDescriptor = NULL
		ri.sa.bInheritHandle = TRUE
		CreatePipe(@ri.ReadPipe, @ri.WritePipe, @ri.sa, 0)
		' 创建进程
		GetStartupInfo(@si)
		si.hStdInput = ri.ReadPipe
		si.hStdOutput = ri.WritePipe
		si.hStdError = ri.WritePipe
		si.dwFlags = STARTF_USESTDHANDLES
		si.wShowWindow = iShow
		CreateProcess(NULL, sPath, NULL, NULL, TRUE, 0, NULL, NULL, @si, @pi)
		CloseHandle(ri.WritePipe)
		' 从管道读取数据
		Dim buf As ZString * 4096
		Dim bytesRead As UInteger
		AutoFreeBuffer.FreeMemory()
		Do
			Dim RetInt As Integer = ReadFile(ri.ReadPipe, @buf, 4095, @bytesRead, NULL)
			If RetInt Then
				AutoFreeBuffer.PutData(@buf, bytesRead)
			Else
				Exit Do
			EndIf
		Loop
		CloseHandle(ri.ReadPipe)
		Return AutoFreeBuffer.BufferMemory
	End Function
	
	Sub DoEvents() Export
		xRtl.DoEvents()
	End Sub
	
	Function xHiByte Alias "HiByte" (i16 As Integer) As Integer Export
		Return HiByte(i16)
	End Function
	
	Function xLoByte Alias "LoByte" (i16 As Integer) As Integer Export
		Return LoByte(i16)
	End Function
	
	Function xHiWord Alias "HiWord" (i32 As Integer) As Integer Export
		Return HiWord(i32)
	End Function
	
	Function xLoWord Alias "LoWord" (i32 As Integer) As Integer Export
		Return LoWord(i32)
	End Function
	
	Function HiInt(i64 As LongInt) As Integer Export
		Return (i64 Shr 32) And &HFFFFFFFF
	End Function
	
	Function LoInt(i64 As LongInt) As Integer Export
		Return i64 And &HFFFFFFFF
	End Function
	
	Function MakeInt(Lo16 As Integer, Hi16 As Integer) As Integer Export
		Return MAKELONG(Lo16, Hi16)
	End Function
	
	Function MakeSrt(Lo8 As Integer, Hi8 As Integer) As Integer Export
		Return MAKEWORD(Lo8, Hi8)
	End Function
	
	Function MakeLng(lo32 As Integer, Hi32 As Integer) As LongInt Export
		Dim LoLng As LongInt = lo32
		Dim HiLng As LongInt = Hi32
		Return LoLng Or (HiLng Shl 32)
	End Function
	
	
	
	Function Rect_GetCenterX(pRect As RECT Ptr) As Integer Export
		Return pRect->Left + ((pRect->Right - pRect->Left) / 2)
	End Function
	
	Function Rect_GetCenterY(pRect As RECT Ptr) As Integer Export
		Return pRect->top + ((pRect->bottom - pRect->top) / 2)
	End Function
	
	Function Rect_GetRandomX(pRect As RECT Ptr) As Integer Export
		Dim w As Integer = pRect->Right - pRect->Left
		Return pRect->Left + (Rnd() * w)
	End Function
	
	Function Rect_GetRandomY(pRect As RECT Ptr) As Integer Export
		Dim h As Integer = pRect->bottom - pRect->top
		Return pRect->top + (Rnd() * h)
	End Function
	
	Function Rect_Left(pRect As RECT Ptr) As Integer Export
		Return pRect->Left
	End Function
	
	Function Rect_Right(pRect As RECT Ptr) As Integer Export
		Return pRect->Right
	End Function
	
	Function Rect_Top(pRect As RECT Ptr) As Integer Export
		Return pRect->Top
	End Function
	
	Function Rect_Bottom(pRect As RECT Ptr) As Integer Export
		Return pRect->bottom
	End Function
	
	Function Rect_Width(pRect As RECT Ptr) As Integer Export
		Return pRect->Right - pRect->Left
	End Function
	
	Function Rect_Height(pRect As RECT Ptr) As Integer Export
		Return pRect->bottom - pRect->Top
	End Function
	
	
	
	' 改变当前进程权限
	Function AdjustPrivilege() As Integer Export
		Dim hToken As HANDLE
		If OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY, @hToken) Then
			Dim tp As TOKEN_PRIVILEGES
			Dim luid As LUID
			If LookupPrivilegeValue(NULL, SE_DEBUG_NAME, @.Luid) Then
				tp.PrivilegeCount = 1
				tp.Privileges(0).Luid = Luid
				tp.Privileges(0).Attributes = SE_PRIVILEGE_ENABLED
				AdjustTokenPrivileges(hToken, FALSE, @tp, SizeOf(tp), NULL, NULL)
			EndIf
			Function = iif(GetLastError() = ERROR_SUCCESS, -1, 0)
			CloseHandle(hToken)
		EndIf
	End Function
	
	
	
	' 获取系统目录
	Function GetSystemPath(idx As Integer) As ZString Ptr Export
		Dim pidl As Any Ptr
		Dim sRet As UByte Ptr = xRtl.TempMemory(MAX_PATH)
		*Cast(ZString Ptr, sRet) = xRtl.xywh_library_xrtl_memory_snull
		Select Case idx
			Case 1
				' System32 目录
				GetSystemDirectory(sRet, MAX_PATH)
			Case 2
				' 软件安装目录
				*Cast(ZString Ptr, sRet) = Environ("Programfiles")
			Case 3
				' 我的文档
				SHGetSpecialFolderLocation(0, CSIDL_PERSONAL, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 4
				' Temp 目录
				GetTempPath(MAX_PATH, sRet)
			Case 5
				' 字体目录
				SHGetSpecialFolderLocation(0, CSIDL_FONTS, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 6
				' AppData 目录
				SHGetSpecialFolderLocation(0, CSIDL_APPDATA, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 7
				' 桌面目录（当前用户）
				SHGetSpecialFolderLocation(0, CSIDL_DESKTOP, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 8
				' 桌面目录（所有用户）
				SHGetSpecialFolderLocation(0, CSIDL_COMMON_DESKTOPDIRECTORY, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 9
				' 开始菜单（当前用户）
				SHGetSpecialFolderLocation(0, CSIDL_STARTMENU, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 10
				' 开始菜单（所有用户）
				SHGetSpecialFolderLocation(0, CSIDL_COMMON_STARTMENU, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 11
				' 程序组（当前用户）
				SHGetSpecialFolderLocation(0, CSIDL_PROGRAMS, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 12
				' 程序组（所有用户）
				SHGetSpecialFolderLocation(0, CSIDL_COMMON_PROGRAMS, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 13
				' 启动目录（当前用户）
				SHGetSpecialFolderLocation(0, CSIDL_STARTUP, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 14
				' 启动目录（所有用户）
				SHGetSpecialFolderLocation(0, CSIDL_COMMON_STARTUP, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 15
				' 收藏夹（当前用户）
				SHGetSpecialFolderLocation(0, CSIDL_FAVORITES, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 16
				' 收藏夹（所有用户）
				SHGetSpecialFolderLocation(0, CSIDL_COMMON_FAVORITES, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 17
				' 网页缓存
				SHGetSpecialFolderLocation(0, CSIDL_INTERNET_CACHE, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 18
				' 历史记录
				SHGetSpecialFolderLocation(0, CSIDL_HISTORY, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 19
				' Cookies 目录
				SHGetSpecialFolderLocation(0, CSIDL_COOKIES, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 20
				' 历史记录
				SHGetSpecialFolderLocation(0, CSIDL_RECENT, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 21
				' 发送到
				SHGetSpecialFolderLocation(0, CSIDL_SENDTO, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case 22
				' 模版目录
				SHGetSpecialFolderLocation(0, CSIDL_TEMPLATES, @pidl)
				SHGetPathFromIDList(pidl, sRet)
			Case Else
				' Windows 目录
				GetWindowsDirectory(sRet, MAX_PATH)
		End Select
		' 如果目录不以 \ 结尾则补充
		Dim iSize As Integer = strlen(sRet)
		if sRet[iSize - 1] <> 92 Then
			sRet[iSize] = 92
			sRet[iSize + 1] = 0
		EndIf
		Return sRet
	End Function
	
	' 获取路径中的目录部分
	Function GetPathDir(sPath As ZString Ptr) As ZString Ptr Export
		If PathFileExists(sPath) Then
			If PathIsDirectory(sPath) Then
				Return sPath
			Else
				Dim iSize As Integer = strlen(sPath)
				Dim NewPath As ZString Ptr = xRtl.TempMemory(iSize + 1)
				memcpy(NewPath, sPath, iSize + 1)
				PathRemoveFileSpec(NewPath)
				Return NewPath
			EndIf
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取路径中的文件部分
	Function GetPathFile(sPath As ZString Ptr) As ZString Ptr Export
		If PathFileExists(sPath) Then
			If PathIsDirectory(sPath) Then
				Return @xRtl.xywh_library_xrtl_memory_snull
			Else
				Dim iSize As Integer = strlen(sPath)
				Dim NewPath As ZString Ptr = xRtl.TempMemory(iSize + 1)
				memcpy(NewPath, sPath, iSize + 1)
				PathStripPath(NewPath)
				Return NewPath
			EndIf
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
	' 获取绝对路径
	Function GetPathAbs(sPath As ZString Ptr) As ZString Ptr Export
		If PathIsRelative(sPath) Then
			Dim sDest As ZString Ptr = xRtl.TempMemory(MAX_PATH)
			PathCombine(sDest, CurDir(), sPath)
			Return sDest
		Else
			Return sPath
		EndIf
	End Function
	
	' 获取相对路径
	Function GetPathRel(sDir1 As ZString Ptr, sDir2 As ZString Ptr) As ZString Ptr Export
		Dim sDest As ZString Ptr = xRtl.TempMemory(MAX_PATH)
		Dim iArrDir1 As Integer = IIf(PathIsDirectory(sDir1), FILE_ATTRIBUTE_DIRECTORY, 0)
		Dim iArrDir2 As Integer = IIf(PathIsDirectory(sDir2), FILE_ATTRIBUTE_DIRECTORY, 0)
		PathRelativePathTo(sDest, sDir1, iArrDir1, sDir2, iArrDir2)
		Return sDest
	End Function
	
	' 路径缩短
	Function GetPathPixel(sPath As ZString Ptr, hWin As HANDLE, iPixel As UInteger) As ZString Ptr Export
		Dim iSize As UInteger = strlen(sPath)
		Dim sNewPath As ZString Ptr = xRtl.TempMemory(iSize + 1)
		lstrcpy(sNewPath, sPath)
		Dim hDC As HDC = GetDC(hWin)
		PathCompactPath(hDC, sNewPath, iPixel)
		ReleaseDC(hWin, hDC)
		Return sNewPath
	End Function
	
	' 路径缩短
	Function GetPathPixelDC(sPath As ZString Ptr, hDC As HANDLE, iPixel As UInteger) As ZString Ptr Export
		Dim iSize As UInteger = strlen(sPath)
		Dim sNewPath As ZString Ptr = xRtl.TempMemory(iSize + 1)
		lstrcpy(sNewPath, sPath)
		PathCompactPath(hDC, sNewPath, iPixel)
		Return sNewPath
	End Function
	
	' 获取随机路径
	Function GetRandPath(sPathHead As ZString Ptr, sPathFoot As ZString, iRandSize As Integer) As ZString Ptr Export
		AutoFreeBuffer.FreeMemory()
		AutoFreeBuffer.PutData(sPathHead)
		For i As Integer = 1 To iRandSize
			AutoFreeBuffer.PutData(Str(CInt(Rnd() * 9)), 1)
		Next
		AutoFreeBuffer.PutData(sPathFoot)
		Return AutoFreeBuffer.BufferMemory
	End Function
	
	
	
	/'
	Dim Shared sRet As UByte Ptr
	Function Replace(sText As ZString Ptr, sSub As ZString Ptr, sRep As ZString Ptr, iLenText As UInteger = 0, iLenSub As UInteger = 0, iLenRep As UInteger = 0) As ZString Ptr
		' 数据长度计算
		If iLenText = 0 Then
			iLenText = strlen(sText)
		EndIf
		If iLenSub = 0 Then
			iLenSub = strlen(sSub)
		EndIf
		If iLenRep = 0 Then
			iLenRep = strlen(sRep)
		EndIf
		' 开始处理
		If iLenText AndAlso iLenSub AndAlso iLenRep Then
			Dim sArr As UByte Ptr = sText
			Dim char As UByte = Cast(UByte Ptr, sSub)[0]
			Dim iPos As UByte Ptr = sText
			'Dim sRet As UByte Ptr = malloc(iLenRep / iLenSub * iLenText)
			Dim pRet As UByte Ptr = sRet
			Dim iSize As UInteger
			For i As Integer = 0 To iLenText - 1
				If sArr[i] = char Then
					If memcmp(@sArr[i], sSub, iLenSub) = 0 Then
						iSize = @sArr[i] - iPos
						memcpy(pRet, iPos, iSize)
						memcpy(pRet + iSize, sRep, iLenRep)
						pRet += iSize + iLenRep
						iPos = @sArr[i] + iLenSub
						i += iLenSub - 1
					EndIf
				EndIf
			Next
			If @sArr[iLenText] <> iPos Then
				memcpy(pRet, iPos, @sArr[iLenText] - iPos)
				pRet[@sArr[iLenText] - iPos] = 0
			EndIf
			Return sRet
		EndIf
	End Function
	'/
End Extern
