


Type xClient
	h_Client As HANDLE
	Event_Proc As HANDLE
	Event_Recv As HANDLE
	ThreadLock As Any Ptr
End Type



' 客户端共用回调
Sub xSock_TCP_Client_Proc(Hdr As Integer, Event As Integer, lpOverlapped As Iocp_Link, Size As Integer, Custom As Integer, pClient As xClient Ptr)
	Select Case Event
		Case IOCP_TCP_RECV
			If pClient->Event_Proc Then
				Dim RecvPack As ZString Ptr
				RecvPack = TCP_Read(lpOverlapped)
				If RecvPack Then
					If pClient->Event_Recv Then
						AutoFreeBuffer.FreeMemory()
						AutoFreeBuffer.PutData(RecvPack, Size)
						SendMessage(pClient->Event_Recv, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					EndIf
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(!"Recv", 4)
					SendMessage(pClient->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(pClient->Event_Proc, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case IOCP_TCP_SEND
			If pClient->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData("Send_Succ", 9)
				SendMessage(pClient->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(pClient->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case IOCP_TCP_ESEND
			If pClient->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData("Send_Fail", 9)
				SendMessage(pClient->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(pClient->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case IOCP_TCP_DISCONNECT
			If pClient->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData("Disconnect", 10)
				SendMessage(pClient->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(pClient->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
	End Select
End Sub



' 引用计数
Dim Shared xSock_Client_RefCount As Integer
' 最大客户端数量
Dim Shared xSock_Client_MaxCount As Integer = 16
' 最大线程数量
Dim Shared xSock_Client_ThreadCount As Integer = 1
' 初始化状态
Dim Shared xSock_Client_InitState As Integer
' 全局的 Socket 句柄
Dim Shared xSock_Client_hSocket As HANDLE



' 初始化 [自动增加引用计数]
Function xSock_Client_Init() As HANDLE
	If xSock_Client_InitState = FALSE Then
		xSock_Client_hSocket = TCP_Client_Create(NULL, @xSock_TCP_Client_Proc, xSock_Client_MaxCount, xSock_Client_ThreadCount, 0)
	EndIf
	If xSock_Client_hSocket Then
		xSock_Client_RefCount += 1
		xSock_Client_InitState = TRUE
		Return xSock_Client_hSocket
	EndIf
End Function

' 卸载 [引用计数为0才真正卸载]
Sub xSock_Client_Unit()
	xSock_Client_RefCount -= 1
	If xSock_Client_RefCount <= 0 Then
		If xSock_Client_hSocket Then
			TCP_Client_Destroy(xSock_Client_hSocket)
			xSock_Client_hSocket = NULL
		EndIf
		xSock_Client_InitState = FALSE
	EndIf
End Sub



Extern "Windows-MS"
	
	' 设置最大客户端数量
	Sub Client_SetMaxCount(c As UInteger) Export
		xSock_Client_MaxCount = c
	End Sub
	
	' 设置最大线程数量
	Sub Client_SetThreadCount(c As UInteger) Export
		xSock_Client_ThreadCount = c
	End Sub
	
	Function Client_Create(ip As ZString Ptr, Port As Integer, EventProc As HANDLE, EventRecv As HANDLE) As Any Ptr Export
		Dim ClassPtr As xClient Ptr = calloc(1, SizeOf(xClient))
		If ClassPtr Then
			ClassPtr->Event_Proc = EventProc
			ClassPtr->Event_Recv = EventRecv
			Dim hSocket As HANDLE = xSock_Client_Init()
			If hSocket Then
				ClassPtr->h_Client = TCP_Client_ConnectStart(hSocket, ip, Port, ClassPtr, TRUE)
				If ClassPtr->h_Client Then
					ClassPtr->ThreadLock = MutexCreate()
					TCP_Client_ConnectEnd(hSocket, ClassPtr->h_Client)
					Return ClassPtr
				Else
					free(ClassPtr)
					xSock_Client_Unit()
					Return NULL
				EndIf
			EndIf
		EndIf
	End Function
	
	Sub Client_Destroy(hSock As xClient Ptr) Export
		If hSock Then
			MutexLock(hSock->ThreadLock)
			TCP_CloseSocket(hSock->h_Client)
			MutexUnLock(hSock->ThreadLock)
			MutexDestroy(hSock->ThreadLock)
			xSock_Client_Unit()
			free(hSock)
		EndIf
	End Sub
	
	Function Client_Send(hSock As xClient Ptr, sPack As ZString Ptr, iSize As UInteger, Sync As Integer) As Integer Export
		If hSock Then
			If iSize = 0 Then
				iSize = strlen(sPack)
			EndIf
			MutexLock(hSock->ThreadLock)
			Function = TCP_Write(hSock->h_Client, sPack, iSize, Sync)
			MutexUnLock(hSock->ThreadLock)
		EndIf
	End Function
	
End Extern
