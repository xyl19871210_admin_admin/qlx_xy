


/'
#Inclib "LoadImage"

Declare Function BitmapFromFile Alias "BitmapFromFile" (ByVal FileName As ZString Ptr) As HBITMAP
Declare Function BitmapFromMemory Alias "BitmapFromMemory" (ByVal Buffer As Any Ptr,ByVal BufferSize As Integer) As HBITMAP
Declare Function BitmapFromResource Alias "BitmapFromResource" (ByVal ModHdr As HANDLE,ByVal NameStr As ZString Ptr) As HBITMAP
'/



' 字体类定义
#Define XUI_FONT_BOLD			1
#Define XUI_FONT_ITALIC			2
#Define XUI_FONT_UNDERLINE		4
#Define XUI_FONT_STRIKEOUT		8



Extern "Windows-MS"
	
	Function Paint_Create(hParent As HANDLE, x As Integer, y As Integer, w As Integer, h As Integer, Event As HANDLE, EventFlag As Integer) As HANDLE Export
		Dim ClassPtr As PaintCon_Data Ptr = calloc(1, SizeOf(PaintCon_Data))
		If ClassPtr Then
			ClassPtr->Event = Event
			ClassPtr->EventFlag = EventFlag
			Dim RetHdr As HANDLE = CreateDialogParam(hInst, Cast(ZString Ptr, 1180), hParent, @PaintConProc, Cast(Integer, ClassPtr))
			SetWindowPos(RetHdr, 0, x, y, w, h, SWP_NOZORDER Or SWP_NOACTIVATE)
			Return RetHdr
		EndIf
	End Function
	
	Sub Paint_Destroy(hPaint As HANDLE) Export
		SendMessage(hPaint, WM_CLOSE, 0, 0)
	End Sub
	
	' 获取 HDC
	Function Paint_HDC(hPaint As HANDLE) As HDC Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Return ClassPtr->ImgHdc
		EndIf
	End Function
	
	' 获取 HBITMAP
	Function Paint_HBITMAP(hPaint As HANDLE) As HBITMAP Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Return ClassPtr->ImgBmp
		EndIf
	End Function
	
	' 改变画布大小
	Sub Paint_Size(hPaint As HANDLE, w As Integer, h As Integer) Export
		SetWindowPos(hPaint, 0, 0, 0, w, h, SWP_NOACTIVATE Or SWP_NOMOVE Or SWP_NOZORDER)
	End Sub
	
	' 清屏
	Sub Paint_Clear(hPaint As HANDLE, c As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Dim tRect As RECT = (0, 0, ClassPtr->ImgW, ClassPtr->ImgH)
			Dim hBrush As HBRUSH = CreateSolidBrush(c)
			FillRect(ClassPtr->ImgHdc, @tRect, hBrush)
			DeleteObject(hBrush)
		EndIf
	End Sub
	
	' 刷新到画面
	Sub Paint_Flip(hPaint As HANDLE) Export
		InvalidateRect(hPaint, NULL, TRUE)
		UpdateWindow(hPaint)
	End Sub
	
	' 画点
	Sub Paint_PSet(hPaint As HANDLE, x As Integer, y As Integer, c As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			SetPixel(ClassPtr->ImgHdc, x, y, c)
		EndIf
	End Sub
	
	' 画线
	Sub Paint_Line(hPaint As HANDLE, x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, c As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Dim gPen As HPEN = CreatePen(PS_SOLID, 1, c)
			Dim OldPen As HANDLE = SelectObject(ClassPtr->ImgHdc, gPen)
			MoveToEx(ClassPtr->ImgHdc, x1, y1, NULL)
			LineTo(ClassPtr->ImgHdc, x2, y2)
			SelectObject(ClassPtr->ImgHdc, OldPen)
			DeleteObject(gPen)
		EndIf
	End Sub
	
	' 画矩形
	Sub Paint_Rect(hPaint As HANDLE, x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, c As Integer, bFill As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Dim tRect As RECT = (x1, y1, x2, y2)
			Dim hBrush As HBRUSH = CreateSolidBrush(c)
			If bFill Then
				FillRect(ClassPtr->ImgHdc, @tRect, hBrush)
			Else
				FrameRect(ClassPtr->ImgHdc, @tRect, hBrush)
			EndIf
			DeleteObject(hBrush)
		EndIf
	End Sub
	
	' 画圆形
	Sub Paint_Circ(hPaint As HANDLE, x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, c As Integer, bFill As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Dim gPen As HPEN = CreatePen(PS_SOLID, 1, c)
			Dim OldPen As HANDLE = SelectObject(ClassPtr->ImgHdc, gPen)
			If bFill Then
				Dim hBrush As HBRUSH = CreateSolidBrush(c)
				Dim OldBrush As HANDLE = SelectObject(ClassPtr->ImgHdc, hBrush)
				Ellipse(ClassPtr->ImgHdc, x1, y1, x2, y2)
				SelectObject(ClassPtr->ImgHdc, OldBrush)
				DeleteObject(hBrush)
			Else
				Ellipse(ClassPtr->ImgHdc, x1, y1, x2, y2)
			EndIf
			SelectObject(ClassPtr->ImgHdc, OldPen)
			DeleteObject(gPen)
		EndIf
	End Sub
	
	' 写字设置
	Sub Paint_SetFont(hPaint As HANDLE, sFont As ZString Ptr, iSize As Integer, iFlag As Integer, iChar As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			' 释放旧字体
			If (ClassPtr->hFont <> NULL) And (ClassPtr->hFont <> Global_Paint_Font) Then
				DeleteObject(ClassPtr->hFont)
			EndIf
			' 创建新字体
			Dim lfWeight As Integer		= IIf(iFlag And XUI_FONT_BOLD, 700, 400)
			Dim lfItalic As Integer		= IIf(iFlag And XUI_FONT_ITALIC, TRUE, FALSE)
			Dim lfUnderline As Integer	= IIf(iFlag And XUI_FONT_UNDERLINE, TRUE, FALSE)
			Dim lfStrikeOut As Integer	= IIf(iFlag And XUI_FONT_STRIKEOUT, TRUE, FALSE)
			If iChar = 0 Then
				iChar = GB2312_CHARSET
			EndIf
			ClassPtr->hFont = CreateFont(MulDiv(iSize, GetDeviceCaps(ClassPtr->ImgHdc, LOGPIXELSY), 72), 0, 0, 0, lfWeight, lfItalic, lfUnderline, lfStrikeOut, iChar, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, sFont)
			SelectObject(ClassPtr->ImgHdc, ClassPtr->hFont)
		EndIf
	End Sub
	
	' 写字
	Sub Paint_TextOut(hPaint As HANDLE, x As Integer, y As Integer, txt As ZString Ptr, c As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			SetTextColor(ClassPtr->ImgHdc, c)
			SetBkMode(ClassPtr->ImgHdc, TRANSPARENT)
			TextOut(ClassPtr->ImgHdc, x, y, txt, strlen(txt))
		EndIf
	End Sub
	
	' 写字
	Sub Paint_DrawText(hPaint As HANDLE, x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, txt As ZString Ptr, c As Integer, iFormat As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			SetTextColor(ClassPtr->ImgHdc, c)
			SetBkMode(ClassPtr->ImgHdc, TRANSPARENT)
			Dim tr As RECT = (x1, y1, x2, y2)
			DrawText(ClassPtr->ImgHdc, txt, strlen(txt), @tr, iFormat)
		EndIf
	End Sub
	
	' 贴图
	Sub Paint_Image(hPaint As HANDLE, x As Integer, y As Integer, img As ZString Ptr) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			'Dim hBmp As HBITMAP = BitmapFromFile(img)
			Dim hBmp As HBITMAP = LoadImage(NULL, img, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE)
			Dim MemDC As HDC = CreateCompatibleDC(NULL)
			SelectObject(MemDC, hBmp)
			Dim oBmp As BITMAP
			GetObject(hBmp, SizeOf(BITMAP), @oBmp)
			BitBlt(ClassPtr->ImgHdc, x, y, oBmp.bmWidth, oBmp.bmHeight, MemDC, 0, 0, SRCCOPY)
			DeleteObject(hBmp)
			DeleteDC(MemDC)
		EndIf
	End Sub
	
	' 屏幕截图
	Sub Paint_PrintScreen(hPaint As HANDLE, sx As Integer, sy As Integer, sw As Integer, sh As Integer) Export
		Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hPaint, DWL_USER))
		If ClassPtr Then
			Dim udtDevMode As DEVMODE
			Dim SDC As HDC = CreateDC("DISPLAY", NULL, NULL, @udtDevMode)
			StretchBlt(ClassPtr->ImgHdc, 0, 0, ClassPtr->ImgW, ClassPtr->ImgH, SDC, sx, sy, sw, sh, SRCCOPY)
			DeleteDC(SDC)
			Paint_Flip(hPaint)
		EndIf
	End Sub
	
	' 获取拖入文件列表
	Function Paint_GetDrop(hDrop As HANDLE) As ZString Ptr Export
		If hDrop Then
			Dim Count As UInteger = DragQueryFile(hDrop, Cast(UINT, -1), NULL, 0)
			Dim sPath As ZString Ptr = malloc(MAX_PATH)
			AutoFreeBuffer.FreeMemory()
			For i As Integer = 0 To Count - 1
				Dim iSize As UInteger = DragQueryFile(hDrop, i, sPath, MAX_PATH)
				AutoFreeBuffer.PutData(sPath, iSize)
				AutoFreeBuffer.PutData(!"\r\n", 2)
			Next
			free(sPath)
			DragFinish(hDrop)
			If AutoFreeBuffer.BufferMemory Then
				AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen - 2] = 0
				Return AutoFreeBuffer.BufferMemory
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
End Extern
