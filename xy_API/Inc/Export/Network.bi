


#include "win\iphlpapi.bi"



Dim Shared IcmpCreateFile As Function() As HANDLE
Dim Shared IcmpCloseHandle As Function(Hdr As HANDLE) As Integer
Dim Shared IcmpSendEcho As Function(IcmpHandle As HANDLE, DestAddress As Integer, RequestData As Any Ptr, RequestSize As Integer, RequestOptns As Any Ptr, ReplyBuffer As Any Ptr, ReplySize As Integer, TimeOut As Integer) As Integer



Extern "Windows-MS"
	
	Function Ping(IP As ZString Ptr, ot As UInteger = 1000) As Integer Export
		Function = -1
		If (IcmpCreateFile <> NULL) And (IcmpCloseHandle <> NULL) And (IcmpSendEcho <> NULL) Then
			Dim hIcmp As HANDLE = IcmpCreateFile()
			If hIcmp Then
				Dim SendData As ZString * 32 = "xywhsoft test ping ..."
				Dim ReplySize As UInteger = SizeOf(ICMP_ECHO_REPLY) + 32
				Dim ReplyBuffer As LPVOID = malloc(ReplySize)
				' ��������
				Dim iat As Integer = inet_addr(IP)
				If iat = INADDR_NONE Then
					Dim HostEntry As hostent Ptr = gethostbyname(IP)
					If HostEntry Then
						iat = Cast(in_addr Ptr, HostEntry->h_addr)->s_addr
					EndIf
				EndIf
				If iat <> INADDR_NONE Then
					If IcmpSendEcho(hIcmp, iat, @SendData, SizeOf(SendData), NULL, ReplyBuffer, ReplySize, ot) Then
						Dim pEchoReply As PICMP_ECHO_REPLY = Cast(PICMP_ECHO_REPLY, ReplyBuffer)
						Function = pEchoReply->RoundTripTime
					EndIf
					free(ReplyBuffer)
				EndIf
				IcmpCloseHandle(hIcmp)
			EndIf
		EndIf
	End Function
	
	Function Ping_Port(IP As ZString Ptr, Port As Integer) As Integer Export
		Function = -1
		If Global_Wsk_Init = NO_ERROR Then
			Dim client As SOCKET = socket_(AF_INET,SOCK_STREAM,IPPROTO_TCP)
			If client <> INVALID_SOCKET Then
				Dim clientService As sockaddr_in
				clientService.sin_family = AF_INET
				clientService.sin_addr.s_addr = inet_addr(IP)
				clientService.sin_port = htons(Port)
				' ��������
				If clientService.sin_addr.s_addr = INADDR_NONE Then
					Dim HostEntry As hostent Ptr = gethostbyname(IP)
					If HostEntry Then
						clientService.sin_addr.s_addr = Cast(in_addr Ptr, HostEntry->h_addr)->s_addr
					EndIf
				EndIf
				' �����ӳ�
				Dim NowTimer As Integer = xNow()
				If Connect(client,Cast(PSOCKADDR,@clientService),SizeOf(clientService)) <> SOCKET_ERROR Then
					Function = xNow() - NowTimer
				EndIf
				CloseSocket(client)
			EndIf
		EndIf
	End Function
	
	Function Network_LocalIP() As ZString Ptr Export
		Dim sLocalName As ZString * 64
		If gethostname(@sLocalName, 64) = 0 Then
			Dim pHostInfo As hostent Ptr
			pHostInfo = gethostbyname(@sLocalName)
			If pHostInfo Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData(Str(Cast(UByte Ptr, pHostInfo->h_addr_list[0])[0]), 0)
				AutoFreeBuffer.PutData(".", 1)
				AutoFreeBuffer.PutData(Str(Cast(UByte Ptr, pHostInfo->h_addr_list[0])[1]), 0)
				AutoFreeBuffer.PutData(".", 1)
				AutoFreeBuffer.PutData(Str(Cast(UByte Ptr, pHostInfo->h_addr_list[0])[2]), 0)
				AutoFreeBuffer.PutData(".", 1)
				AutoFreeBuffer.PutData(Str(Cast(UByte Ptr, pHostInfo->h_addr_list[0])[3]), 0)
				Return AutoFreeBuffer.BufferMemory
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	
End Extern
