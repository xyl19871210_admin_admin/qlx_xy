


Extern "Windows-MS"
	
	' 获取线程TID
	/'
	Function Thread_H2ID(Hdr As HANDLE) As Integer Export
		Return GetThreadId(Hdr)
	End Function
	'/
	
	' 线程状态
	Function Thread_Status(tHdr As HANDLE) As Integer Export
		Dim ExitCode As Integer
		If GetExitCodeThread(tHdr, @ExitCode) Then
			If ExitCode = STILL_ACTIVE Then
				Return -1
			EndIf
		EndIf
	End Function
	Function TID_Status(tid As Integer) As Integer Export
		Dim hThread As HANDLE = OpenThread(THREAD_QUERY_INFORMATION, FALSE, tid)
		If hThread Then
			Function = Thread_Status(hThread)
			CloseHandle(hThread)
		EndIf
	End Function
	
	' 暂停线程	[Wow64SuspendThread]
	/'
	Function Thread_Pause(tHdr As HANDLE) As Integer Export
		Return SuspendThread(tHdr)
	End Function
	'/
	
	' 结束线程
	Function Thread_Kill(tHdr As HANDLE) As Integer Export
		If tHdr Then
			TerminateThread(tHdr, 0)
			Return WaitForSingleObject(tHdr, INFINITE)
		EndIf
	End Function
	Function TID_Kill(tid As Integer) As Integer Export
		Dim hThread As HANDLE = OpenThread(THREAD_TERMINATE, FALSE, tid)
		If hThread Then
			TerminateThread(hThread, 0)
			Function = WaitForSingleObject(hThread, INFINITE)
			CloseHandle(hThread)
		EndIf
	End Function
	
End Extern
