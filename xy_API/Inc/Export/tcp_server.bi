


Type xServer_Client
	h_Client As HANDLE
	ip As ZString * 16
	Port As Integer
End Type

Type xServer
	h_Socket As HANDLE
	Event_Proc As HANDLE
	Event_Recv As HANDLE
	ThreadLock As Any Ptr
	c_Max As UInteger
	c_Conn As UInteger
	c_Enter As UInteger
	c_Leave As UInteger
	c_List As xServer_Client Ptr
	c_AddIndex As UInteger
End Type



' 添加到客户端列表
Function xServer_AddClientList(hServer As xServer Ptr, hClient As HANDLE) As Integer
	' 记录客户端句柄到列表
	If hServer->c_AddIndex < hServer->c_Max Then
		Dim cinfo As ClientInfo
		TCP_Server_GetClientInfo(hServer->h_Socket, hClient, @cinfo)
		hServer->c_List[hServer->c_AddIndex].h_Client = hClient
		hServer->c_List[hServer->c_AddIndex].ip = *cinfo.ip
		hServer->c_List[hServer->c_AddIndex].Port = cinfo.port
		hServer->c_AddIndex += 1
		hServer->c_Enter += 1
		hServer->c_Conn += 1
		' 事件处理 [连接]
		If hServer->Event_Proc Then
			AutoFreeBuffer.FreeMemory()
			AutoFreeBuffer.PutData(!"Accept\r\n", 8)
			AutoFreeBuffer.PutData(Str(hClient))
			AutoFreeBuffer.PutData(!"\r\n", 2)
			AutoFreeBuffer.PutData(cinfo.ip)
			AutoFreeBuffer.PutData(":", 1)
			AutoFreeBuffer.PutData(Str(cinfo.port))
			SendMessage(hServer->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
			SendMessage(hServer->Event_Proc, WM_LBUTTONDOWN, 0, 0)
		EndIf
		Return TRUE
	Else
		' 句柄表已经用完 [循环找空闲句柄]
		For i As Integer = 0 To hServer->c_Max - 1
			If hServer->c_List[i].h_Client = NULL Then
				Dim cinfo As ClientInfo
				TCP_Server_GetClientInfo(hServer->h_Socket, hClient, @cinfo)
				hServer->c_List[i].h_Client = hClient
				hServer->c_List[i].ip = *cinfo.ip
				hServer->c_List[i].Port = cinfo.port
				hServer->c_Enter += 1
				hServer->c_Conn += 1
				' 事件处理 [连接]
				If hServer->Event_Proc Then
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(!"Accept\r\n", 8)
					AutoFreeBuffer.PutData(Str(hClient))
					AutoFreeBuffer.PutData(!"\r\n", 2)
					AutoFreeBuffer.PutData(cinfo.ip)
					AutoFreeBuffer.PutData(":", 1)
					AutoFreeBuffer.PutData(Str(cinfo.port))
					SendMessage(hServer->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(hServer->Event_Proc, WM_LBUTTONDOWN, 0, 0)
				EndIf
				Return TRUE
			EndIf
		Next
	EndIf
End Function

' 从客户端列表删除
Function xServer_DelClientList(hServer As xServer Ptr, hClient As HANDLE) As Integer
	For i As Integer = 0 To hServer->c_Max - 1
		If hServer->c_List[i].h_Client = hClient Then
			hServer->c_List[i].h_Client = NULL
			hServer->c_Leave += 1
			hServer->c_Conn -= 1
			' 事件处理 [断开]
			If hServer->Event_Proc Then
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData(!"Disconnect\r\n", 12)
				AutoFreeBuffer.PutData(Str(hClient))
				AutoFreeBuffer.PutData(!"\r\n", 2)
				AutoFreeBuffer.PutData(@hServer->c_List[i].ip)
				AutoFreeBuffer.PutData(":", 1)
				AutoFreeBuffer.PutData(Str(hServer->c_List[i].port))
				SendMessage(hServer->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(hServer->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
			hServer->c_List[i].ip = ""
			hServer->c_List[i].port = 0
			Return TRUE
		EndIf
	Next
End Function

' 服务端共用回调
Sub xSock_TCP_Server_Proc(Hdr As Integer, Event As Integer, lpOverlapped As Iocp_Link, Size As Integer, Custom As xServer Ptr, binddata As Integer)
	Select Case Event
		Case IOCP_TCP_RECV			' 收到消息
			If Custom->Event_Proc Then
				Dim RecvPack As ZString Ptr
				RecvPack = TCP_Read(lpOverlapped)
				If RecvPack Then
					If Custom->Event_Recv Then
						AutoFreeBuffer.FreeMemory()
						AutoFreeBuffer.PutData(RecvPack, Size)
						SendMessage(Custom->Event_Recv, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					EndIf
					Dim cinfo As ClientInfo
					TCP_Server_GetClientInfo(Custom->h_Socket, lpOverlapped, @cinfo)
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(!"Recv\r\n", 6)
					AutoFreeBuffer.PutData(Str(lpOverlapped))
					AutoFreeBuffer.PutData(!"\r\n", 2)
					AutoFreeBuffer.PutData(cinfo.ip)
					AutoFreeBuffer.PutData(":", 1)
					AutoFreeBuffer.PutData(Str(cinfo.port))
					SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case IOCP_TCP_SEND			' 消息发送成功
			If Custom->Event_Proc Then
				Dim cinfo As ClientInfo
				TCP_Server_GetClientInfo(Custom->h_Socket, lpOverlapped, @cinfo)
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData(!"Send_Succ\r\n", 11)
				AutoFreeBuffer.PutData(Str(lpOverlapped))
				AutoFreeBuffer.PutData(!"\r\n", 2)
				AutoFreeBuffer.PutData(cinfo.ip)
				AutoFreeBuffer.PutData(":", 1)
				AutoFreeBuffer.PutData(Str(cinfo.port))
				SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case IOCP_TCP_ESEND			' 消息发送失败
			If Custom->Event_Proc Then
				Dim cinfo As ClientInfo
				TCP_Server_GetClientInfo(Custom->h_Socket, lpOverlapped, @cinfo)
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData(!"Send_Fail\r\n", 11)
				AutoFreeBuffer.PutData(Str(lpOverlapped))
				AutoFreeBuffer.PutData(!"\r\n", 2)
				AutoFreeBuffer.PutData(cinfo.ip)
				AutoFreeBuffer.PutData(":", 1)
				AutoFreeBuffer.PutData(Str(cinfo.port))
				SendMessage(Custom->Event_Proc, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(Custom->Event_Proc, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case IOCP_TCP_ACCEPT		' 新连接
			MutexLock(Custom->ThreadLock)
			If xServer_AddClientList(Custom, lpOverlapped) = FALSE Then
				TCP_CloseSocket(lpOverlapped)
			EndIf
			MutexUnLock(Custom->ThreadLock)
		Case IOCP_TCP_DISCONNECT	' 断开连接
			MutexLock(Custom->ThreadLock)
			xServer_DelClientList(Custom, lpOverlapped)
			MutexUnLock(Custom->ThreadLock)
	End Select
End Sub



Extern "Windows-MS"
	
	Sub Server_Destroy(hServer As xServer Ptr) Export
		If hServer Then
			If hServer->h_Socket Then
				TCP_Server_Destroy(hServer->h_Socket)
			EndIf
			If hServer->c_List Then
				free(hServer->c_List)
			EndIf
			If hServer->ThreadLock Then
				MutexDestroy(hServer->ThreadLock)
			EndIf
			free(hServer)
		EndIf
	End Sub
	
	Function Server_Create(ip As ZString Ptr, Port As Integer, MaxConn As UInteger, ThreadCount As UInteger, EventProc As HANDLE, EventRecv As HANDLE) As Any Ptr Export
		Dim ClassPtr As xServer Ptr = calloc(1, SizeOf(xServer))
		If ClassPtr Then
			ClassPtr->Event_Proc = EventProc
			ClassPtr->Event_Recv = EventRecv
			ClassPtr->h_Socket = TCP_Server_Create(ip, Port, NULL, @xSock_TCP_Server_Proc, MaxConn, ThreadCount, ClassPtr)
			If ClassPtr->h_Socket Then
				ClassPtr->c_List = calloc(MaxConn, SizeOf(xServer_Client))
				If ClassPtr->c_List Then
					ClassPtr->ThreadLock = MutexCreate()
					' 设置统计数据
					ClassPtr->c_Max = MaxConn
					ClassPtr->c_Conn = 0
					ClassPtr->c_Enter = 0
					ClassPtr->c_Leave = 0
					Return ClassPtr
				Else
					Server_Destroy(ClassPtr)
				EndIf
			Else
				Server_Destroy(ClassPtr)
			EndIf
		EndIf
	End Function
	
	Function Server_Send(hServer As xServer Ptr, hClient As HANDLE, sPack As ZString Ptr, iSize As UInteger, Sync As Integer) As Integer Export
		If (hServer <> NULL) And (hClient <> NULL) Then
			If iSize = 0 Then
				iSize = strlen(sPack)
			EndIf
			MutexLock(hServer->ThreadLock)
			Function = TCP_Write(hClient, sPack, iSize, Sync)
			MutexUnLock(hServer->ThreadLock)
		EndIf
	End Function
	
	Function Server_SendAll(hServer As xServer Ptr, sPack As ZString Ptr, iSize As UInteger, Sync As Integer) As Integer Export
		If hServer Then
			If iSize = 0 Then
				iSize = strlen(sPack)
			EndIf
			Dim SendCount As Integer
			For i As Integer = 0 To hServer->c_Max - 1
				If hServer->c_List[i].h_Client Then
					MutexLock(hServer->ThreadLock)
					If TCP_Write(hServer->c_List[i].h_Client, sPack, iSize, Sync) Then
						SendCount += 1
					EndIf
					MutexUnLock(hServer->ThreadLock)
				EndIf
			Next
			Return SendCount
		EndIf
	End Function
	
	Function Server_ListClient(hServer As xServer Ptr) As ZString Ptr Export
		If hServer Then
			AutoFreeBuffer.FreeMemory()
			For i As Integer = 0 To hServer->c_Max - 1
				If hServer->c_List[i].h_Client Then
					AutoFreeBuffer.PutData(Str(hServer->c_List[i].h_Client))
					AutoFreeBuffer.PutData(!":", 1)
					AutoFreeBuffer.PutData(@hServer->c_List[i].ip)
					AutoFreeBuffer.PutData(":", 1)
					AutoFreeBuffer.PutData(Str(hServer->c_List[i].port))
					AutoFreeBuffer.PutData(!"\r\n", 2)
				EndIf
			Next
			If AutoFreeBuffer.BufferMemory Then
				AutoFreeBuffer.BufferMemory[AutoFreeBuffer.BufferUseLen-2] = 0
				Return AutoFreeBuffer.BufferMemory
			Else
				Return @xRtl.xywh_library_xrtl_memory_snull
			EndIf
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	/'
	Function Server_GetClientInfo(hServer As xServer Ptr, hClient As HANDLE) As ZString Ptr Export
		If (hServer <> NULL) And (hClient <> NULL) Then
			Dim cinfo As ClientInfo
			TCP_Server_GetClientInfo(hServer->h_Socket, hClient, @cinfo)
			AutoFreeBuffer.FreeMemory()
			AutoFreeBuffer.PutData(cinfo.ip)
			AutoFreeBuffer.PutData(":", 1)
			AutoFreeBuffer.PutData(Str(cinfo.port))
			Return AutoFreeBuffer.BufferMemory
		Else
			Return @xRtl.xywh_library_xrtl_memory_snull
		EndIf
	End Function
	'/
	Function Server_GetConnCount(hServer As xServer Ptr) As Integer Export
		If hServer Then
			Return hServer->c_Conn
		EndIf
	End Function
	
	Function Server_GetEnterCount(hServer As xServer Ptr) As Integer Export
		If hServer Then
			Return hServer->c_Enter
		EndIf
	End Function
	
	Function Server_GetLeaveCount(hServer As xServer Ptr) As Integer Export
		If hServer Then
			Return hServer->c_Leave
		EndIf
	End Function
	
	Function Server_CloseClient(hServer As xServer Ptr, hClient As HANDLE) As Integer Export
		If (hServer <> NULL) And (hClient <> NULL) Then
			If xServer_DelClientList(hServer, hClient) Then
				TCP_CloseSocket(hClient)
				Return -1
			EndIf
		EndIf
	End Function
	
End Extern
