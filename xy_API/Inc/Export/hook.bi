


Dim Shared As HHOOK HookK, HookM
Dim Shared As HWND  hWinK, hWinM



Function HookProcK(Code As Integer, wParam As Integer, lParam As PKEYBDINPUT) As Integer
	If hWinK Then
		Dim MsgStr As ZString * 64
		MsgStr = Str(Code) & "|" & Str(wParam) & "|" & Str(lParam->wVk) & "|" & Str(lParam->wScan) & "|" & Str(lParam->dwFlags) & "|" & Str(lParam->Time) & "|" & Str(lParam->dwExtraInfo)
		SendMessage(hWinK, WM_SETTEXT, 0, Cast(Integer, @MsgStr))
		SendMessage(hWinK, WM_LBUTTONDOWN, 0, 0)
	EndIf
	Return 0
End Function

Function HookProcM(Code As Integer, wParam As Integer, lParam As PMOUSEINPUT) As Integer
	If hWinM Then
		Dim MsgStr As ZString * 64
		MsgStr = Str(Code) & "|" & Str(wParam) & "|" & Str(lParam->dx) & "|" & Str(lParam->dy) & "|" & Str(Cast(Short, HiWord(lParam->mouseData))) & "|" & Str(lParam->dwFlags) & "|" & Str(lParam->Time)
		SendMessage(hWinM, WM_SETTEXT, 0, Cast(Integer, @MsgStr))
		SendMessage(hWinM, WM_LBUTTONDOWN, 0, 0)
	EndIf
	Return 0
End Function



Extern "Windows-MS"
	
	Sub Hook_DelKey_LowLevel() Export
		If HookK Then
			UnhookWindowsHookEx(HookK)
			HookK = NULL
			hWinK = NULL
		EndIf
	End Sub
	
	Sub Hook_DelMouse_LowLevel() Export
		If HookM Then
			UnhookWindowsHookEx(HookM)
			HookM = NULL
			hWinM = NULL
		EndIf
	End Sub
	
	Sub Hook_InsKey_LowLevel(Event As HANDLE) Export
		Hook_DelKey_LowLevel()
		hWinK = Event
		HookK = SetWindowsHookEx(WH_KEYBOARD_LL, Cast(Any Ptr, @HookProcK), hInst, 0)
	End Sub
	
	Sub Hook_InsMouse_LowLevel(Event As HANDLE) Export
		Hook_DelMouse_LowLevel()
		hWinM = Event
		HookM = SetWindowsHookEx(WH_MOUSE_LL, Cast(Any Ptr, @HookProcM), hInst, 0)
	End Sub
	
End Extern
