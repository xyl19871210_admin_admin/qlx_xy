


Extern "Windows-MS"
	
	Sub Key_Press(hWin As HWND, vKey As Integer) Export
		If hWin Then
			SendMessage(hWin, WM_KEYDOWN, vKey, 0)
			SendMessage(hWin, WM_KEYUP, vKey, 0)
		Else
			Dim TI(1) As INPUT_
			TI(0).ki.dwFlags = 0
			TI(0).ki.wVK = vKey
			TI(0).ki.wScan = MapVirtualKey(vKey, 0)
			TI(0).ki.time = GetTickCount()
			TI(0).ki.dwExtraInfo = GetMessageExtraInfo()
			TI(0).Type = INPUT_KEYBOARD
			TI(1).ki.dwFlags = KEYEVENTF_KEYUP
			TI(1).ki.wVK = vKey
			TI(1).ki.wScan = MapVirtualKey(vKey, 0)
			TI(1).ki.time = GetTickCount()
			TI(1).ki.dwExtraInfo = GetMessageExtraInfo()
			TI(1).Type = INPUT_KEYBOARD
			SendInput(2, @TI(0), SizeOf(TI))
		EndIf
	End Sub
	
	Sub Key_Down(hWin As HWND, vKey As Integer) Export
		If hWin Then
			SendMessage(hWin, WM_KEYDOWN, vKey, 0)
		Else
			Dim TI As INPUT_
			TI.ki.dwFlags = 0
			TI.ki.wVK = vKey
			TI.ki.wScan = MapVirtualKey(vKey, 0)
			TI.ki.time = GetTickCount()
			TI.ki.dwExtraInfo = GetMessageExtraInfo()
			TI.Type = INPUT_KEYBOARD
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
	Sub Key_Up(hWin As HWND, vKey As Integer) Export
		If hWin Then
			SendMessage(hWin, WM_KEYUP, vKey, 0)
		Else
			Dim TI As INPUT_
			TI.ki.dwFlags = KEYEVENTF_KEYUP
			TI.ki.wVK = vKey
			TI.ki.wScan = MapVirtualKey(vKey, 0)
			TI.ki.time = GetTickCount()
			TI.ki.dwExtraInfo = GetMessageExtraInfo()
			TI.Type = INPUT_KEYBOARD
			SendInput(1, @TI, SizeOf(TI))
		EndIf
	End Sub
	
End Extern
