


Type Menu_Data
	WinHandle As HANDLE
	MenuHandle As HANDLE
	EventHandle As HANDLE
End Type



Function MenuProc(ByVal hWin As HWND, ByVal uMsg As UINT, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As Integer
	' 窗口消息处理
	Select Case uMsg
		Case WM_INITDIALOG
			' 窗口初始化
			SetWindowLong(hWin, DWL_USER, lParam)
			Cast(Menu_Data Ptr, lParam)->WinHandle = hWin
		Case WM_COMMAND
			If HiWord(wParam) = BN_CLICKED Then
				Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
				If ClassPtr->EventHandle Then
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(Str(LoWord(wParam)))
					SendMessage(ClassPtr->EventHandle, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(ClassPtr->EventHandle, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case WM_CLOSE
			Dim ClassPtr As Menu_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			DestroyMenu(ClassPtr->MenuHandle)
			free(ClassPtr)
			DestroyWindow(hWin)
		Case Else
			Return FALSE
	End Select
	Return TRUE
End Function


