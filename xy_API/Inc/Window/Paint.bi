


#Define GET_X_LPARAM(lp) (Cast(Integer, Cast(Short, LoWord(lp))))
#Define GET_Y_LPARAM(lp) (Cast(Integer, Cast(Short, HiWord(lp))))



Type PaintCon_Data
	WinHandle As HANDLE
	ImgW As UShort
	ImgH As UShort
	ImgHdc As HDC
	ImgBmp As HBITMAP
	hFont As HANDLE
	Event As HANDLE
	EventFlag As Integer
End Type



Extern "Windows-MS"
	Declare Sub Paint_Clear(hPaint As HANDLE, c As Integer)
End Extern



Function PaintConProc(ByVal hWin As HWND, ByVal uMsg As UINT, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As Integer
	Dim As Long id, Event
	Select Case uMsg
		Case WM_LBUTTONDOWN, WM_LBUTTONUP, WM_LBUTTONDBLCLK, WM_RBUTTONDOWN, WM_RBUTTONUP, WM_MBUTTONDOWN, WM_MBUTTONUP, WM_MOUSEWHEEL, WM_MOUSEMOVE
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->Event Then
				If uMsg = WM_MOUSEMOVE Then
					If ClassPtr->EventFlag And 1 Then
						Return TRUE
					EndIf
				Else
					If ClassPtr->EventFlag And 2 Then
						Return TRUE
					EndIf
				EndIf
				AutoFreeBuffer.FreeMemory()
				AutoFreeBuffer.PutData(Str(uMsg))
				AutoFreeBuffer.PutData(",", 1)
				AutoFreeBuffer.PutData(Str(GET_X_LPARAM(lParam)))
				AutoFreeBuffer.PutData(",", 1)
				AutoFreeBuffer.PutData(Str(GET_Y_LPARAM(lParam)))
				AutoFreeBuffer.PutData(",", 1)
				AutoFreeBuffer.PutData(Str(wParam))
				SendMessage(ClassPtr->Event, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
				SendMessage(ClassPtr->Event, WM_LBUTTONDOWN, 0, 0)
			EndIf
		Case WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, WM_SYSKEYUP, WM_CHAR, WM_DEADCHAR, WM_SYSCHAR, WM_SYSDEADCHAR
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->Event Then
				If (ClassPtr->EventFlag And 4) = 0 Then
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(Str(uMsg))
					AutoFreeBuffer.PutData(",", 1)
					AutoFreeBuffer.PutData(Str(wParam))
					AutoFreeBuffer.PutData(",", 1)
					AutoFreeBuffer.PutData(Str(lParam))
					SendMessage(ClassPtr->Event, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(ClassPtr->Event, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case WM_DROPFILES
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->Event Then
				If (ClassPtr->EventFlag And 8) = 0 Then
					Dim pt As Point
					DragQueryPoint(Cast(HDROP, wParam), @pt)
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(Str(uMsg))
					AutoFreeBuffer.PutData(",", 1)
					AutoFreeBuffer.PutData(Str(pt.x))
					AutoFreeBuffer.PutData(",", 1)
					AutoFreeBuffer.PutData(Str(pt.y))
					AutoFreeBuffer.PutData(",", 1)
					AutoFreeBuffer.PutData(Str(wParam))
					SendMessage(ClassPtr->Event, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(ClassPtr->Event, WM_LBUTTONDOWN, 0, 0)
				EndIf
			EndIf
		Case WM_INITDIALOG
			' 窗口初始化
			Dim ClassPtr As PaintCon_Data Ptr = Cast(PaintCon_Data Ptr, lParam)
			SetWindowLong(hWin, DWL_USER, lParam)
			ClassPtr->WinHandle = hWin
			' 创建默认字体 [宋体9号]
			If Global_Paint_Font = NULL Then
				Dim hDC As HDC = GetDC(hWin)
				Global_Paint_Font = CreateFont(MulDiv(9, GetDeviceCaps(hDC, LOGPIXELSY), 72), 0, 0, 0, 400, FALSE, FALSE, FALSE, GB2312_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "宋体")
				ReleaseDC(hWin, hDC)
			EndIf
			ClassPtr->hFont = Global_Paint_Font
			' 添加消息过滤
			Dim hUser32 As HANDLE = LoadLibrary("user32.dll")
			Dim ChangeWindowMessageFilter As Function(uMsg As UInteger, dwFlag As UInteger) As BOOL
			ChangeWindowMessageFilter = Cast(Any Ptr, GetProcAddress(hUser32, "ChangeWindowMessageFilter"))
			If ChangeWindowMessageFilter Then
				ChangeWindowMessageFilter(WM_DROPFILES, 1)		' 1 = MSGFLT_ADD、2 = MSGFLT_REMOVE
				ChangeWindowMessageFilter(&H0049, 1)			' &H0049 = WM_COPYGLOBALDATA
			EndIf
		Case WM_PAINT
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			Dim ps As PAINTSTRUCT
			ps.hdc = BeginPaint(hWin, @PS)
				BitBlt(ps.hdc, ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.Right - ps.rcPaint.left, ps.rcPaint.bottom - ps.rcPaint.top, ClassPtr->ImgHdc, ps.rcPaint.left, ps.rcPaint.top, SRCCOPY)
			EndPaint(hWin, @PS)
		Case WM_SIZE
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			Dim w As Integer = LoWord(lParam)
			Dim h As Integer = HiWord(lParam)
			' 创建新画面
			Dim hDC As HDC = GetDC(hWin)
			Dim TmpHDC As HDC = CreateCompatibleDC(hDC)
			Dim TmpBMP As HBITMAP = CreateCompatibleBitmap(hDC, w, h)
			SelectObject(TmpHDC, TmpBMP)
			' 新画面清屏
			Dim tRect As RECT = (0, 0, w, h)
			Dim hBrush As HBRUSH = CreateSolidBrush(&HFFFFFF)
			FillRect(TmpHDC, @tRect, hBrush)
			DeleteObject(hBrush)
			' 旧画面拷贝到新画面
			If ClassPtr->ImgHdc Then
				BitBlt(TmpHDC, 0, 0, ClassPtr->ImgW, ClassPtr->ImgH, ClassPtr->ImgHdc, 0, 0, SRCCOPY)
				' 释放旧画面
				DeleteDC(ClassPtr->ImgHdc)
				DeleteObject(ClassPtr->ImgBmp)
			EndIf
			' 设置值
			SelectObject(TmpHDC, ClassPtr->hFont)
			ClassPtr->ImgHdc = TmpHDC
			ClassPtr->ImgBmp = TmpBMP
			ClassPtr->ImgW = w
			ClassPtr->ImgH = h
			ReleaseDC(hWin, hDC)
		Case WM_CLOSE
			Dim ClassPtr As PaintCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If (ClassPtr->hFont <> NULL) And (ClassPtr->hFont <> Global_Paint_Font) Then
				DeleteObject(ClassPtr->hFont)
			EndIf
			DeleteDC(ClassPtr->ImgHdc)
			DeleteObject(ClassPtr->ImgBmp)
			free(ClassPtr)
			DestroyWindow(hWin)
		Case Else
			Return FALSE
	End Select
	Return TRUE
End Function

