


Type SpyCon_Data
	WinHandle As HANDLE
	ImgHandle As HANDLE
	AimHandle As HANDLE
	Event_Move As HANDLE
	Event_Over As HANDLE
	IsCapture As Integer
End Type



Dim Shared As HANDLE Spy_AimNull, Spy_AimFull
Dim Shared As HANDLE Spy_AimCur , Spy_DefCur



Function SpyToolProc(ByVal hWin As HWND, ByVal uMsg As UINT, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As Integer
	Static As Integer IsCapture
	' 窗口消息处理
	Select Case uMsg
		Case WM_INITDIALOG
			' 窗口初始化
			SetWindowLong(hWin, DWL_USER, lParam)
			Cast(SpyCon_Data Ptr, lParam)->WinHandle = hWin
			Cast(SpyCon_Data Ptr, lParam)->ImgHandle = GetDlgItem(hWin, 1191)
			If Spy_AimFull = NULL Then Spy_AimFull = LoadBitmap(hInst, Cast(Any Ptr, 201))
			If Spy_AimNull = NULL Then Spy_AimNull = LoadBitmap(hInst, Cast(Any Ptr, 202))
			If Spy_AimCur = NULL Then Spy_AimCur  = LoadCursor(hInst, Cast(Any Ptr, 200))
			If Spy_DefCur = NULL Then Spy_DefCur  = Cast(HCURSOR, GetClassLong(hWin, GCL_HCURSOR))
		Case WM_LBUTTONDOWN
			' 鼠标按下
			Dim ClassPtr As SpyCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->IsCapture = FALSE Then
				ClassPtr->IsCapture = TRUE
				' 改变图像和指针，开启鼠标独占
				SendMessage(ClassPtr->ImgHandle, STM_SETIMAGE, IMAGE_BITMAP, Cast(Integer, Spy_AimNull))
				SetCapture(hWin)
				SetCursor(Spy_AimCur)
			EndIf
		Case WM_LBUTTONUP
			' 鼠标弹起
			Dim ClassPtr As SpyCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->IsCapture Then
				ClassPtr->IsCapture = FALSE
				' 改变图像和指针，释放鼠标独占
				SendMessage(ClassPtr->ImgHandle, STM_SETIMAGE, IMAGE_BITMAP, Cast(Integer, Spy_AimFull))
				SetCursor(Spy_DefCur)
				If ClassPtr->Event_Over Then
					AutoFreeBuffer.FreeMemory()
					AutoFreeBuffer.PutData(Str(ClassPtr->AimHandle))
					SendMessage(ClassPtr->Event_Over, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
					SendMessage(ClassPtr->Event_Over, WM_LBUTTONDOWN, 0, 0)
				EndIf
				ReleaseCapture()
			EndIf
		Case WM_MOUSEMOVE
			' 鼠标移动
			Dim ClassPtr As SpyCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			If ClassPtr->IsCapture Then
				' 定位窗口
				Dim pt As Point
				GetCursorPos(@pt)
				Dim ConAim As HWND = WindowFromPoint(pt)
				If ConAim <> ClassPtr->AimHandle Then
					ClassPtr->AimHandle = ConAim
					If ClassPtr->Event_Move Then
						AutoFreeBuffer.FreeMemory()
						AutoFreeBuffer.PutData(Str(ClassPtr->AimHandle))
						SendMessage(ClassPtr->Event_Move, WM_SETTEXT, 0, Cast(Integer, AutoFreeBuffer.BufferMemory))
						SendMessage(ClassPtr->Event_Move, WM_LBUTTONDOWN, 0, 0)
					EndIf
				EndIf
			EndIf
		Case WM_CLOSE
			Dim ClassPtr As SpyCon_Data Ptr = Cast(Any Ptr, GetWindowLong(hWin, DWL_USER))
			free(ClassPtr)
			DestroyWindow(hWin)
		Case Else
			Return FALSE
	End Select
	Return TRUE
End Function


