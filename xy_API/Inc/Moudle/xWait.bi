


/' 无实用性
Function xIsPreciseTime() As Integer
	Dim SP As ULongInt
	If QueryPerformanceFrequency(Cast(Any Ptr,@SP)) Then Return -1
End Function

Function xWait(WaitTimer As Integer) As Integer
	Dim SP As ULongInt
	Dim BOL As Integer = QueryPerformanceFrequency(Cast(Any Ptr,@SP))
	If BOL Then
		' 精确模式
		Dim As ULongInt FastST,FastOT
		QueryPerformanceCounter(Cast(Any Ptr,@FastST))
		'Print "最低精确度 : 1/" & SP * 10000 & " 秒"
		Do
			QueryPerformanceCounter(Cast(Any Ptr,@FastOT))
		Loop While (FastOT-FastST)/SP*1000 < WaitTimer
		Return (FastOT-FastST)/SP*1000
	Else
		' 兼容模式
		Dim As Integer SafeST,SafeOT
		SafeST = GetTickCount()
		Do
			SafeOT = GetTickCount()
		Loop While (SafeOT-SafeST) < WaitTimer
		Return SafeOT-SafeST
	EndIf
End Function
'/

Function xNow() As Integer
	Dim SP As ULongInt
	If QueryPerformanceFrequency(Cast(Any Ptr,@SP)) Then
		' 精确模式
		Dim NowTimer As ULongInt
		QueryPerformanceCounter(Cast(Any Ptr, @NowTimer))
		Return NowTimer/SP*1000
	Else
		' 兼容模式
		Return GetTickCount()
	EndIf
End Function
