'==================================================================================
    '★ xywh Buffer 自增长缓冲区类
	'#-----------------------------------------------------------------------------
	'# 功能 : 
	'# 说明 : 
'==================================================================================



' 是否线程安全 [注释掉则为非线程安全版本]
#Define xBuffer_UseThreadLock



Type xBuffer
	
	' 缓冲区内存指针
	BufferMemory As ZString Ptr
	
	' 缓冲区申请大小
	BufferLenght As UInteger
	
	' 缓冲区使用大小
	BufferUseLen As UInteger
	
	' 预分配内存步长
	AllocStep As UInteger
	
	' 线程安全锁
	#Ifdef xBuffer_UseThreadLock
		ThreadLock As Any Ptr
	#EndIf
	
	
	
	' 构造函数
	Declare Constructor(AllocSize As UInteger)
	Declare Constructor()
	
	' 析构函数
	Declare Destructor()
	
	
	
	' 分配内存
	Declare Function AllocMemory(AllocSize As UInteger) As Integer
	
	' 释放内存
	Declare Sub FreeMemory()
	
	
	
	' 写入数据
	Declare Function PutData(pBuffer As ZString Ptr, uSize As UInteger = 0) As Integer
	
End Type





' 构造函数
Constructor xBuffer(AllocSize As UInteger)
	AllocStep = &H10000
	#Ifdef xBuffer_UseThreadLock
		ThreadLock = MutexCreate()
	#EndIf
	AllocMemory(AllocSize)
End Constructor
Constructor xBuffer()
	AllocStep = &H10000
	#Ifdef xBuffer_UseThreadLock
		ThreadLock = MutexCreate()
	#EndIf
End Constructor

' 析构函数
Destructor xBuffer()
	FreeMemory()
End Destructor



' 分配内存
Function xBuffer.AllocMemory(AllocSize As UInteger) As Integer
	#Ifdef xBuffer_UseThreadLock
		MutexLock(ThreadLock)
	#EndIf
	If AllocSize > BufferLenght Then
		' 增量
		Dim NewMem As Any Ptr = realloc(BufferMemory, AllocSize)
		If NewMem Then
			BufferLenght = AllocSize
			BufferMemory = NewMem
		Else
			#Ifdef xBuffer_UseThreadLock
				MutexUnLock(ThreadLock)
			#EndIf
			Return 0
		EndIf
	ElseIf AllocSize < BufferLenght Then
		' 裁剪
		Dim NewMem As Any Ptr = realloc(BufferMemory, AllocSize)
		If NewMem Then
			BufferLenght = AllocSize
			BufferMemory = NewMem
			If AllocSize < BufferUseLen Then	' 裁剪数据，\0截断
				BufferUseLen = AllocSize - 1
				BufferMemory[AllocSize - 1] = 0
			EndIf
		Else
			#Ifdef xBuffer_UseThreadLock
				MutexUnLock(ThreadLock)
			#EndIf
			Return 0
		EndIf
	ElseIf AllocSize = 0 Then
		' 清空
		FreeMemory()
	EndIf
	#Ifdef xBuffer_UseThreadLock
		MutexUnLock(ThreadLock)
	#EndIf
	Return -1
End Function

' 释放内存
Sub xBuffer.FreeMemory()
	If BufferMemory Then
		DeAllocate(BufferMemory)
		BufferMemory = NULL
	EndIf
	BufferLenght = 0
	BufferUseLen = 0
End Sub



' 写入数据
Function xBuffer.PutData(pBuffer As ZString Ptr, uSize As UInteger = 0) As Integer
	' 自动计算数据长度
	If uSize = 0 Then
		uSize = strlen(pBuffer)
	EndIf
	If uSize Then
		' 是否需要自增长
		If BufferUseLen + uSize >= BufferLenght Then
			If AllocMemory(BufferUseLen + uSize + AllocStep) = 0 Then
				Return 0
			EndIf
		EndIf
		' 写入数据，\0截断
		#Ifdef xBuffer_UseThreadLock
			MutexLock(ThreadLock)
		#EndIf
		memcpy(BufferMemory + BufferUseLen, pBuffer, uSize)
		BufferUseLen += uSize
		BufferMemory[BufferUseLen] = 0
		#Ifdef xBuffer_UseThreadLock
			MutexUnLock(ThreadLock)
		#EndIf
	EndIf
	Return -1
End Function
