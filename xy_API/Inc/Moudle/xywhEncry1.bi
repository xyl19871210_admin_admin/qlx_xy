#Include Once "windows.bi"



'#Define DebugMode				' 去掉注释则Dump相关文件


#Ifdef DebugMode
	#Ifndef xywh_IsFile
		Function PutFile(ByVal FileName As ZString Ptr,ByVal OutBuffer As Any Ptr,ByVal StartPoint As Integer,ByVal OutLenght As Integer) As Integer
			#Ifdef LogOutPut
				AddLog("	PutFile	函数执行 , 参数列表 : FileName[" & *FileName & "]  StartPoint[" & StartPoint & "]  OutLenght[" & OutLenght & "]")
			#EndIf
			Dim As HANDLE File_HANDLE = CreateFile(FileName,GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL)
			If File_HANDLE = INVALID_HANDLE_VALUE Then
				#Ifdef LogOutPut
					AddLog("Error : [CreateFile] , GetLastError : " & GetLastError())
				#EndIf
				PutFile = GetLastError()
			Else
				Dim As Integer oRet = SetFilePointer(File_HANDLE,StartPoint,0,FILE_BEGIN)
				If oRet = -1 Then
					#Ifdef LogOutPut
						AddLog("Error : [SetFilePointer] , GetLastError : " & GetLastError())
					#EndIf
					PutFile = GetLastError()
				Else
					Dim TOL As OVERLAPPED
					TOL.Offset = oRet
					If WriteFile(File_HANDLE,OutBuffer,OutLenght,NULL,@TOL) Then
						PutFile = 0
					Else
						#Ifdef LogOutPut
							AddLog("Error : [WriteFile] , GetLastError : " & GetLastError())
						#EndIf
						PutFile = GetLastError()
					EndIf
				EndIf
			EndIf
			CloseHandle(File_HANDLE)
		End Function
	#EndIf
#EndIf



Type IntToByte
	B1 As Byte
	B2 As Byte
	B3 As Byte
	B4 As Byte
End Type

Type IntToShort
	S1 As Short
	S2 As Short
End Type



Sub xywhEncry1(ByVal DataPtr As Any Ptr,ByVal DataSize As Integer,ByVal Paaword As ZString Ptr,ByVal PwSize As Integer)
	Dim SBox(1023) As Integer		' S盒[用于密钥的二次混淆]
	Dim KBox(63) As Integer			' K盒[用于密钥的一次混淆]
	Dim PwInt As Integer				' 二次混淆因子[用于S盒混淆的运算因子,取自K盒]
	Dim PwStep As Integer				' 原始密码的步数[一步=4字节]
	Dim DtStep As Integer				' 数据的计算步数
	Dim RAM As Any Ptr					' 临时内存
	Dim i As Integer						' 循环变量
	Dim TmpInt As UInteger			' 临时Int变量
	
	PwStep = (PwSize + 3) \ 4		' 首先算出密码的步数[一步=4字节]
	RAM = Allocate(PwStep*4)		' 将密码复制到临时容器中[为了末尾\0处理]
	CopyMemory(RAM,Paaword,PwSize)
	
	For i = 0 To 63							' 使用密码填满“密码容器”
		CopyMemory(@KBox(i),((i Mod PwStep) * 4) + RAM,4)
	Next
	DeAllocate(RAM)
	
	For i = 0 To 63							' 进行第一次对密钥的伪随机混淆[K盒混淆]
		Dim TB As IntToByte Ptr
		TB = Cast(Any Ptr,@KBox(i))
		TmpInt = (TmpInt + TB->B1) And &HFF
		TB->B1 = TmpInt
		TmpInt = (TmpInt + TB->B2) And &HFF
		TB->B2 = TmpInt
		TmpInt = (TmpInt + TB->B3) And &HFF
		TB->B3 = TmpInt
		TmpInt = (TmpInt + TB->B4) And &HFF
		TB->B4 = TmpInt
		If i Then PwInt = KBox(i-1) Xor KBox(i)	' 为第二次密钥伪随机混淆采集因子
	Next
	#Ifdef DebugMode
		putfile(ExePath & "\Dump-KBox.txt",@KBox(0),0,256)
	#EndIf
	
	For i = 0 To 1023							' 进行第二次密钥混淆[S盒混淆]
		Dim TB As IntToShort Ptr
		PwInt = PwInt Xor KBox(i And 63)
		TB = Cast(Any Ptr,@PwInt)
		TmpInt = (TmpInt + TB->S1) And &HFFFF
		TB->S1 = TmpInt
		TmpInt = (TmpInt + TB->S2) And &HFFFF
		TB->S2 = TmpInt
		SBox(i) = PwInt
	Next
	#Ifdef DebugMode
		PutFile(ExePath & "\Dump-SBox.txt",@SBox(0),0,4096)
	#EndIf
	
	DtStep = (DataSize + 3) \ 4		' 计算加密需要进行的次数[四字节对齐]
	Dim DataOut As Integer Ptr
	For i = 0 To DtStep-1					' 开始加密
		DataOut = DataPtr + (4 * i)
		SBox(i And 1023) += SBox((i+1) And 1023)
		*DataOut = *DataOut Xor SBox(i And 1023)
	Next
	#Ifdef DebugMode
		PutFile(ExePath & "\Dump-All.txt",DataPtr,0,DataSize)
	#EndIf
End Sub
