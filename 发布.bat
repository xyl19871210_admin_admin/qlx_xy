@echo 更新库文件
@echo;
@copy /B /Y ".\xy_API\xyLib.dll" ".\发布目录\星月API扩展库\xyapi.dll"
@copy /B /Y ".\神梦插件\SMWH.dll" ".\发布目录\神梦插件\SMWH.dll"
@copy /B /Y ".\山海UI库\山海UI库.qml" ".\发布目录\山海UI库\山海UI库.qml"

@echo;
@echo;
@echo;

@echo 更新帮助文件
@echo;
@copy /B /Y ".\帮助文档\帮助文档.chm" ".\发布目录\帮助手册.chm"

@echo;
@echo;
@pause