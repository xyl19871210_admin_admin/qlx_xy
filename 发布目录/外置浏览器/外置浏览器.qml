[General]
SyntaxVersion=2
MacroID=5eb0b093-d2ae-45bf-ae7f-074eb2c38c94
[Comment]

[Script]

Declare Function IsWindow Lib "user32.dll" (ByVal hwnd As Long) As Long
Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Function SendMsgText Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As String) As Long
Declare Function SendMsgRefs Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByRef lParam As String) As Long
Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, ByVal lpsz2 As Long) As Long
Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, ByRef lpdwProcessId As Long) As Long
Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Declare Function GetWindowRect Lib "user32.dll" (ByVal hWnd As Long, ByVal lpRect As Any) As Long



Global G_Path_Dir, G_Path_Exe, G_Path_Ini
Global WebBox()



// 外置浏览器初始化
Function 初始化(sDir)
	If CStr(sDir) = "" Then sDir = GetResPath()
	Dim tChr = Right(sDir, 1)
	If (tChr <> "/") And (tChr <> "\\") Then sDir = sDir & "\\"
	Dim Exe_Path = sDir & "WebBox.exe"
	If PathFileExists(Exe_Path) <> 0 Then
		G_Path_Dir = sDir
		G_Path_Exe = Exe_Path
		G_Path_Ini = sDir & "WebBox.ini"
		WebBox = Array()
		Return true
	Else
		Return false
	End If
End Function

// 外置浏览器创建
Function 创建()
	// 运行外置浏览器
	Dim PID = RunApp(G_Path_Exe)
	If PID = 0 Then 
		Return false, "程序启动失败！"
	End If
	// 等待外置浏览器窗口加载
	Dim hWin_PID = 0, hWin = 0, hWeb = 0
	Dim ST = Timer()
	Do
		Delay 60
		Do
			hWin = FindWindowEx(0, hWin, "ThunderRT6FormDC", 0)
			GetWindowThreadProcessId(hWin, hWin_PID)
			If hWin_PID = PID Then
				hWeb = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 1, 0)
				If hWeb <> 0 Then
					Exit Do
				Else 
					hWin = 0
				End If
			End If
		Loop Until hWin = 0
		If hWin <> 0 Then Exit Do
	Loop While ((Timer() - ST) < 10)
	// 配置外置浏览器数据
	If hWin = 0 Then
		Return false, "超时！"
	Else
		WebBox[hWin] = Array()
		WebBox[hWin]["PID"] = PID
		WebBox[hWin]["hWin"] = hWin
		WebBox[hWin]["hWeb"] = hWeb
		WebBox[hWin]["hParam1"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 101, 0)
		WebBox[hWin]["hParam2"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 102, 0)
		WebBox[hWin]["hParam3"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 103, 0)
		WebBox[hWin]["hParam4"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 104, 0)
		Return true, hWin
	End If
End Function

// 外置浏览器绑定
Function 绑定(hWin)
	If WebBox[hWin] = null Then
		// 绑定新的外置浏览器
		Dim hWeb = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 1, 0)
		If hWeb <> 0 Then
			Dim PID
			GetWindowThreadProcessId(hWin, PID)
			WebBox[hWin] = Array()
			WebBox[hWin]["PID"] = PID
			WebBox[hWin]["hWin"] = hWin
			WebBox[hWin]["hWeb"] = hWeb
			WebBox[hWin]["hParam1"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 101, 0)
			WebBox[hWin]["hParam2"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 102, 0)
			WebBox[hWin]["hParam3"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 103, 0)
			WebBox[hWin]["hParam4"] = SendMessage(hWin, &H1400 /* WB_GETHWND = WM_USER + &H1000 */ , 104, 0)
			Return true
		Else
			Return false
		End If
	Else 
		// 已经绑定过，直接返回 true
		Return true
	End If
End Function

// 外置浏览器是否还在运行
Function 是否还在运行(hWin)
	If CLng(hWin) = 0 Then
		Return false
	Else
		If IsWindow(hWin) = 0 Then
			释放(hWin)
			Return false
		Else
			Return true
		End If
	End If
End Function

// 显示
Function 显示(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1401 /* WB_SHOW = WM_USER + &H1001 */ , 0, 0)
		Return true
	End If
End Function

// 隐藏
Function 隐藏(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1402 /* WB_HIDE = WM_USER + &H1002 */ , 0, 0)
		Return true
	End If
End Function

// 退出
Function 关闭(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1403 /* WB_EXIT = WM_USER + &H1003 */ , 0, 0)
		WebBox[hWin] = null
		Return true
	End If
End Function

// 全部退出
Function 关闭全部()
	Dim Count = 0
	For Each k, v In WebBox
		SendMessage(k, &H1403 /* WB_EXIT = WM_USER + &H1003 */ , 0, 0)
		WebBox[k] = null
		Count = Count + 1
	Next
	Return Count
End Function

// 释放
Function 释放(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		WebBox[hWin] = null
		Return true
	End If
End Function

// 全部释放
Function 释放全部()
	Dim Count = 0
	For Each k, v In WebBox
		WebBox[k] = null
		Count = Count + 1
	Next
	Return Count
End Function

// 设置窗口大小
Function 设置窗口大小(hWin, w, h)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1500 /* WB_WINSIZE = WM_USER + &H1100 */ , w, h)
		Return true
	End If
End Function

// 设置网页大小
Function 设置网页大小(hWin, w, h)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1501 /* WB_WEBSIZE = WM_USER + &H1101 */ , w, h)
		Return true
	End If
End Function

// 获取窗口大小
Function 获取窗口大小(hWin)
	If WebBox[hWin] = null Then
		Return false, 0, 0
	Else
		Dim Rect = StructPack("llll", 0, 0, 0, 0)
		GetWindowRect(hWin, Rect)
		Dim x1, y1, x2, y2
		x1, y1, x2, y2 = StructUnPack("llll", Rect)
		Return true, x2 - x1, y2 - y1
	End If
End Function

// 获取网页大小
Function 获取网页大小(hWin)
	If WebBox[hWin] = null Then
		Return false, 0, 0
	Else
		Dim w = SendMessage(hWin, &H1503 /* WB_GETWEBW = WM_USER + &H1103 */ , 0, 0)
		Dim h = SendMessage(hWin, &H1504 /* WB_GETWEBH = WM_USER + &H1104 */ , 0, 0)
		Return true, w, h
	End If
End Function

// 获取网页大小
Function 获取页面大小(hWin)
	If WebBox[hWin] = null Then
		Return false, 0, 0
	Else
		Dim w = SendMessage(hWin, &H1613 /* WB_GETSCROLLW = WM_USER + &H1213 */ , 0, 0)
		Dim h = SendMessage(hWin, &H1614 /* WB_GETSCROLLH = WM_USER + &H1214 */ , 0, 0)
		Return true, w, h
	End If
End Function

// 获取窗口标题
Function 获取窗口标题(hWin)
	Dim iSize = SendMessage(hWin, 14 /* WM_GETTEXTLENGTH = 14 */ , 0, 0)
	Dim iText = String(iSize + 1)
	SendMsgRefs(hWin, 13 /* WM_GETTEXT */ , iSize + 1, iText)
	Return iText
End Function

// 设置窗口标题
Function 设置窗口标题(hWin, sTitle)
	Return SendMsgText(hWin, 12 /* WM_SETTEXT = 12 */ , 0, sTitle)
End Function

// 获取网页标题
Function 获取网页标题(hWin)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMessage(hWin, &H1620 /* WB_GETTITLE = WM_USER + &H1220 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
		Return true, RetStr
	End If
End Function

// 加载网页
Function 加载页面(hWin, url, ele, flag, ot)
	If WebBox[hWin] = null Then
		Return 0
	Else
		SendMsgText(WebBox[hWin]["hParam2"], 12 /* WM_SETTEXT = 12 */ , 0, url)
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, ele)
		Return SendMessage(hWin, &H2400 /* WB_LOAD = WM_USER + &H2000 */ , flag, ot)
	End If
End Function

// 等待网页加载
Function 等待加载(hWin, ele, flag, ot)
	If WebBox[hWin] = null Then
		Return 0
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, ele)
		Return SendMessage(hWin, &H2401 /* WB_WAIT = WM_USER + &H2001 */ , flag, ot)
	End If
End Function

// 保存网页
Function 保存页面(hWin, path)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, path)
		SendMessage(hWin, &H2500 /* WB_SAVE = WM_USER + &H2100 */ , 0, 0)
		Return true
	End If
End Function

// 运行JS
Function 运行JS(hWin, Script)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, Script)
		SendMessage(hWin, &H2600 /* WB_RUNJS = WM_USER + &H2200 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam2"])
		Return true, RetStr
	End If
End Function

// 运行VBS
Function 运行VBS(hWin, Script)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, Script)
		SendMessage(hWin, &H2601 /* WB_RUNVBS = WM_USER + &H2201 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam2"])
		Return true, RetStr
	End If
End Function

// 运行JS [带运行库]
Function 运行JS_带运行库(hWin, Script)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, Script)
		SendMessage(hWin, &H2600 /* WB_RUNJS = WM_USER + &H2200 */ , 1, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam2"])
		Return true, RetStr
	End If
End Function

// 运行VBS [带运行库]
Function 运行VBS_带运行库(hWin, Script)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, Script)
		SendMessage(hWin, &H2601 /* WB_RUNVBS = WM_USER + &H2201 */ , 1, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam2"])
		Return true, RetStr
	End If
End Function

// 设置代理
Function 设置代理(hWin, tpe, addr)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, tpe)
		SendMsgText(WebBox[hWin]["hParam2"], 12 /* WM_SETTEXT = 12 */ , 0, addr)
		SendMessage(hWin, &H3400 /* WB_SETPROXY = WM_USER + &H3000 */ , 0, 0)
		Return true
	End If
End Function

// 取消代理
Function 取消代理(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H3401 /* WB_CLRPROXY = WM_USER + &H3001 */ , 0, 0)
		Return true
	End If
End Function

// 滚动页面到指定位置
Function 设置滚动条位置(hWin, x, y)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1610 /* WB_SETSCROLL = WM_USER + &H1210 */ , x, y)
		Return true
	End If
End Function

// 获取滚动条位置
Function 获取滚动条位置(hWin)
	If WebBox[hWin] = null Then
		Return false, 0, 0
	Else
		Dim x = SendMessage(hWin, &H1611 /* WB_GETSCROLLX = WM_USER + &H1211 */ , 0, 0)
		Dim y = SendMessage(hWin, &H1612 /* WB_GETSCROLLY = WM_USER + &H1212 */ , 0, 0)
		Return true, x, y
	End If
End Function

// 显示开发者工具
Function 显示开发者工具(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H112 /* WM_SYSCOMMAND = &H112 */ , 2002, 0)
		Return true
	End If
End Function

// 获取选项 [仅运行时]
Function 获取选项(hWin, id)
	If WebBox[hWin] = null Then
		Return false, 0
	Else
		Select Case id
			Case 9, 10
				SendMessage(hWin, &H3500 /* WB_GETGLOBAL = WM_USER + &H3100 */ , id, 0)
				Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
				Return true, RetStr
			Case Else
				Return true, SendMessage(hWin, &H3500 /* WB_GETGLOBAL = WM_USER + &H3100 */ , id, 0)
		End Select
	End If
End Function

// 修改选项 [仅运行时]
Function 修改选项(hWin, id, val)
	If WebBox[hWin] = null Then
		Return false
	Else
		Select Case id
			Case 9, 10
				SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, val)
				SendMessage(hWin, &H3501 /* WB_SETGLOBAL = WM_USER + &H3101 */ , id, 0)
			Case Else
				SendMessage(hWin, &H3501 /* WB_SETGLOBAL = WM_USER + &H3101 */ , id, val)
		End Select
		Return true
	End If
End Function

// 设置工具栏显示状态
Function 显示工具栏(hWin, IsShow)
	Return 修改选项(hWin, 1, IsShow)
End Function

// 跳到链接
Function 跳到链接(hWin, sUrl)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, sUrl)
		SendMessage(hWin, &H1600 /* WB_GOURL = WM_USER + &H1200 */ , 0, 0)
		Return true
	End If
End Function

// 后退
Function 后退(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1601 /* WB_GOBACK = WM_USER + &H1201 */ , 0, 0)
		Return true
	End If
End Function

// 前进
Function 前进(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1602 /* WB_GOFORWARD = WM_USER + &H1202 */ , 0, 0)
		Return true
	End If
End Function

// 打开主页
Function 跳到主页(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1603 /* WB_GOHOME = WM_USER + &H1203 */ , 0, 0)
		Return true
	End If
End Function

// 刷新
Function 刷新(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1604 /* WB_REFRESH = WM_USER + &H1204 */ , 0, 0)
		Return true
	End If
End Function

// 停止
Function 停止(hWin)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMessage(hWin, &H1605 /* WB_STOP = WM_USER + &H1205 */ , 0, 0)
		Return true
	End If
End Function

// 获取网页源代码
Function 获取网页源码(hWin)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMessage(hWin, &H2510 /* WB_GETHTML = WM_USER + &H2110 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
		Return true, RetStr
	End If
End Function

// 获取网页链接
Function 获取URL(hWin)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMessage(hWin, &H2520 /* WB_GETURL = WM_USER + &H2120 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
		Return true, RetStr
	End If
End Function

// 获取本地链接
Function 获取本地URL(hWin)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMessage(hWin, &H2521 /* WB_GETLOCALURL = WM_USER + &H2121 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
		Return true, RetStr
	End If
End Function

// 获取本地Name
Function 获取本地Name(hWin)
	If WebBox[hWin] = null Then
		Return false, ""
	Else
		SendMessage(hWin, &H2522 /* WB_GETLOCALNAME = WM_USER + &H2122 */ , 0, 0)
		Dim RetStr = 获取窗口标题(WebBox[hWin]["hParam1"])
		Return true, RetStr
	End If
End Function

// 完整截图
Function 截图(hWin, sPath)
	If WebBox[hWin] = null Then
		Return false
	Else
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, sPath)
		SendMessage(hWin, &H3601 /* WB_SCREENSHOTFULL = WM_USER + &H3201 */ , 0, 0)
		Return true
	End If
End Function

// 区域截图
Function 区域截图(hWin, x, y, w, h, sPath)
	If WebBox[hWin] = null Then
		Return false
	Else
		Dim wParam = ((x bAnd &HFFFF) bShl 16) bOr (y bAnd &HFFFF)
		Dim lParam = ((w bAnd &HFFFF) bShl 16) bOr (h bAnd &HFFFF)
		SendMsgText(WebBox[hWin]["hParam1"], 12 /* WM_SETTEXT = 12 */ , 0, sPath)
		SendMessage(hWin, &H3600 /* WB_SCREENSHOT = WM_USER + &H3200 */ , wParam, lParam)
		Return true
	End If
End Function

// 获取变量
Function 获取变量(hWin, k)
	If WebBox[hWin] = null Then
		Return null
	Else
		Return WebBox[hWin][k]
	End If
End Function

// 设置变量
Function 设置变量(hWin, k, v)
	If WebBox[hWin] = null Then
		Return false
	Else
		WebBox[hWin][k] = v
		Return true
	End If
End Function

// 获取表
Function 获取数据表(hWin)
	If WebBox[hWin] = null Then
		Return null
	Else
		Return WebBox[hWin]
	End If
End Function





//	xyPath_GenItem
//		基于状态机的词法解析器，通过路径节点描述字符串获取JS代码
//	参数：
//		sItem	: 路径节点描述字符串
//		sParent	: 父界面名字
//	返回值：
//		1. 词法解析状态（逻辑类型，成功或失败）
//		2. JS代码或错误代码（成功返回字符串类型的JS代码，失败返回整数类型的错误代码）
//		3. JS元素类型或错误位置（成功返回逻辑类型的JS元素类型[单个元素还是元素数组]，失败返回出错的识别位置）
//	返回值错误代码：
//		 -1 : 类别标识不合法 (支持 ID、Class、Name、Tag、Child)
//		 -2 : 词法错误，类别标识已经识别
//		 -3 : 词法错误，不正确的结束符号
//		 -4 : 词法错误，类别值不能为空
//		 -5 : 词法错误，括号匹配失败，未开始
//		 -6 : 词法错误，索引值不能为空
//		 -7 : 词法错误，索引值必须为数字
//		 -8 : 词法错误，不支持括号嵌套使用
//		 -9 : 词法错误，无法识别的标识符
//		-10 : 词法错误，括号匹配失败，未结束
//		-11 : 参数错误，类别标识为 Child 时，类别值必须为数字
//	模式位标记代码：
//		 1 : 类别标识是否已经识别过
//		 2 : 是否为括号匹配模式
//		 4 : 是否已经进行过括号匹配
//		 8 : 字符串解析已结束
Function xyPath_GenItem(sItem, sParent, sEleVar)
	Dim char = Right(sItem, 1)
	If char <> "\n" Then sItem = sItem & "\n"
	Dim iSize = Len(sItem)		// 要解析字符串的长度
	Dim text = ""				// 临时字符串缓冲
	Dim Ele_Tpe = "ID"			// 解析后的数据 : 类别标识
	Dim Ele_Val = ""			// 解析后的数据 : 类别值
	Dim Ele_Idx()				// 解析后的数据 : 索引值数组
	Dim Ele_Idx_Count = 0		// 解析后的数据 : 索引值数量
	Dim Mode = 0				// 状态机位
	Dim js = ""					// 生成的JS代码
	Dim IsEles = false			// 返回值是否为元素数组
	// 词法处理和提取
	For i = 1 To iSize
		char = Mid(sItem, i, 1)
		Select Case char
			Case ":"
				If Mode bAnd 1 = 1 Then
					Return false, -2, i
				Else
					text = UCase(text)
					Select Case text
						Case "CLASS", "NAME", "TAG", "CHILD"
							Ele_Tpe = text
							Mode = Mode bOr 1
						Case "ID"
							Ele_Tpe = text
						Case Else
							Return false, -1, i
					End Select
					text = ""
				End If
			Case "(", "["
				If Mode bAnd 1 = 1 Then
					If Mode bAnd 2 = 2 Then
						Return false, -8, i
					Else
						If Mode bAnd 4 = 4 Then
							If Text <> "" Then
								Return false, -9, i
							Else
								Mode = Mode bOr 2
							End If
						Else
							If Text = "" Then
								Return false, -4, i
							Else
								Ele_Val = text
								text = ""
								Mode = Mode bOr 6
							End If
						End If
					End If
				Else
					If Mode bAnd 4 = 4 Then
						Return false, -9, i
					Else
						Ele_Val = text
						text = ""
						Mode = Mode bOr 7
					End If
				End If
			Case ")", "]"
				If Mode bAnd 2 = 2 Then
					If Text = "" Then
						Return false, -6, i
					Else
						If IsNumeric(Text) Then
							Ele_Idx_Count = Ele_Idx_Count + 1
							Ele_Idx[Ele_Idx_Count] = Text
							text = ""
							Mode = Mode bAnd bNot(2)
						Else
							Return false, -7, i
						End If
					End If
				Else
					Return false, -5, i
				End If
			Case "\n"
				If Mode bAnd 2 = 2 Then
					Return false, -10, i
				End If
				If Mode bAnd 4 = 4 Then
					If text <> "" Then 
						Return false, -9, i
					End If
				Else
					Ele_Val = text
					text = ""
					Mode = Mode bOr 5
				End If
				If i <> iSize Then 
					Return false, -3, i
				End If
				Exit For
			Case Else
				text = text & char
		End Select
	Next
	// 参数检查和处理
	If Ele_Tpe = "CHILD" Then 
		If IsNumeric(Ele_Val) = false Then
			Return false, -11, 0
		End If
	End If
	// 根据词法解析器的结果组装新语法
	Select Case Ele_Tpe
		Case "ID"
			js = js & sEleVar & " = " & sParent & ".getElementById(""" & Ele_Val & """);"
			If Ele_Idx_Count > 0 Then
				For i = 1 To Ele_Idx_Count
					'js = js & "\r\n" & sEleVar & " = " & sEleVar & ".childNodes[" & Ele_Idx(i) & "];"
					js = js & "\r\n" & sEleVar & " = " & sEleVar & ".children[" & Ele_Idx(i) & "];"
				Next
			End If
		Case "CLASS"
			js = js & sEleVar & " = " & sParent & ".getElementsByClassName(""" & Ele_Val & """);"
			If Ele_Idx_Count > 0 Then
				js = js & "\r\n" & sEleVar & " = " & sEleVar & "[" & Ele_Idx(1) & "];"
				If Ele_Idx_Count > 1 Then
					For i = 2 To Ele_Idx_Count
						'js = js & "\r\n" & sEleVar & " = " & sEleVar & ".childNodes[" & Ele_Idx(i) & "];"
						js = js & "\r\n" & sEleVar & " = " & sEleVar & ".children[" & Ele_Idx(i) & "];"
					Next
				End If
			Else
				IsEles = true
			End If
		Case "NAME"
			js = js & sEleVar & " = " & sParent & ".getElementsByName(""" & Ele_Val & """);"
			If Ele_Idx_Count > 0 Then
				js = js & "\r\n" & sEleVar & " = " & sEleVar & "[" & Ele_Idx(1) & "];"
				If Ele_Idx_Count > 1 Then
					For i = 2 To Ele_Idx_Count
						'js = js & "\r\n" & sEleVar & " = " & sEleVar & ".childNodes[" & Ele_Idx(i) & "];"
						js = js & "\r\n" & sEleVar & " = " & sEleVar & ".children[" & Ele_Idx(i) & "];"
					Next
				End If
			Else
				IsEles = true
			End If
		Case "TAG"
			js = js & sEleVar & " = " & sParent & ".getElementsByTagName(""" & Ele_Val & """);"
			If Ele_Idx_Count > 0 Then
				js = js & "\r\n" & sEleVar & " = " & sEleVar & "[" & Ele_Idx(1) & "];"
				If Ele_Idx_Count > 1 Then
					For i = 2 To Ele_Idx_Count
						'js = js & "\r\n" & sEleVar & " = " & sEleVar & ".childNodes[" & Ele_Idx(i) & "];"
						js = js & "\r\n" & sEleVar & " = " & sEleVar & ".children[" & Ele_Idx(i) & "];"
					Next
				End If
			Else
				IsEles = true
			End If
		Case "CHILD"
			'js = js & sEleVar & " = " & sParent & ".childNodes[" & Ele_Val & "];"
			js = js & sEleVar & " = " & sParent & ".children[" & Ele_Val & "];"
			If Ele_Idx_Count > 0 Then
				For i = 1 To Ele_Idx_Count
					'js = js & "\r\n" & sEleVar & " = " & sEleVar & ".childNodes[" & Ele_Idx(i) & "];"
					js = js & "\r\n" & sEleVar & " = " & sEleVar & ".children[" & Ele_Idx(i) & "];"
				Next
			End If
	End Select
	Return true, js, IsEles
End Function

//	xyPath_GenPath
//		基于状态机的词法解析器，通过路径描述字符串获取JS代码
//	参数：
//		sPath : 路径描述字符串
//	返回值：
//		1. 词法解析状态（逻辑类型，成功或失败）
//		2. JS代码或错误代码（成功返回字符串类型的JS代码，失败返回整数类型的错误代码）
//		3. JS元素类型或错误位置（成功返回逻辑类型的JS元素类型[单个元素还是元素数组]，失败返回出错的识别位置）
//		4. JS代码的变量名，失败时返回为 null
//	返回值错误代码（支持 xyPath_GenItem 函数的所有错误代码）：
//		-101 : 参数错误，不是最后一个路径节点不允许使用数组
Function xyPath_GenPath(sPath, Root)
	Dim iSize = Len(sPath)			// 要解析字符串的长度
	Dim text = ""					// 临时字符串缓冲
	Dim js = ""						// 生成的JS代码
	Dim IsOK, TempJS, IsEles		// 解析路径节点的返回值
	Dim IsOne = true
	// 生成随机变量名
	Dim VarName = "xyPath_"
	For 6
		VarName = VarName & Rand(0, 9)
	Next
	// 词法处理和提取
	For i = 1 To iSize
		Dim char = Mid(sPath, i, 1)
		Select Case char
			Case "/", "\\"
				If IsOne Then
					IsOK, TempJS, IsEles = xyPath_GenItem(text, Root, VarName)
				Else
					IsOK, TempJS, IsEles = xyPath_GenItem(text, VarName, VarName)
				End If
				If IsOK Then 
					If IsEles Then
						Return false, -101, i
					Else
						If IsOne Then
							js = js & TempJS
						Else
							js = js & "\r\n" & TempJS
						End If
					End If
				Else
					Return false, TempJS, (i - Len(text) - 1) + IsEles
				End If
				IsOne = false
				text = ""
			Case Else
				text = text & char
		End Select
	Next
	// 补充运算
	If IsOne Then
		IsOK, TempJS, IsEles = xyPath_GenItem(text, "document", VarName)
	Else
		IsOK, TempJS, IsEles = xyPath_GenItem(text, VarName, VarName)
	End If
	If IsOK Then
		If IsOne Then
			js = js & TempJS
		Else
			js = js & "\r\n" & TempJS
		End If
	Else
		Return false, TempJS, (iSize - Len(text) - 1) + IsEles
	End If
	Return true, js, IsEles, VarName
End Function



// 基于指定元素运行JS
Function 元素_运行JS(hWin, sPath, sCode)
	If WebBox[hWin] = null Then
		Return false, 0, 0
	Else
		Dim IsOK, js, IsEles, VarName
		IsOK, js, IsEles, VarName = xyPath_GenPath(sPath, "document")
		If IsOK Then 
			If IsEles Then
				js = js & "\r\n" & "for(var i = 0; i < " & VarName & ".length; i++)"
				js = js & "\r\n" & "{"
				js = js & "\r\n" & Replace(sCode, "{$ele}", VarName & "[i]")
				js = js & "\r\n" & "}"
			Else
				js = js & "\r\n" & Replace(sCode, "{$ele}", VarName)
			End If
		End If
		Dim IsOK_JS, JS_Ret
		IsOK_JS, JS_Ret = 运行JS_带运行库(hWin, js)
		If IsOK_JS Then
			Return IsOK, js, IsEles, VarName, JS_Ret
		Else
			Return false, 1, 0
		End If
	End If
End Function

// 判断元素是否存在
Function 元素_是否存在(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "if (isNull({$ele})) {xWebBox_RetVal=0} else {xWebBox_RetVal=1};")
	If IsOK Then
		Return CBool(CLng(JS_Ret)), true
	Else
		Return false, false
	End If
End Function

// 获取一组元素的数量
Function 元素_获取数量(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName = xyPath_GenPath(sPath, "document")
	If IsOK Then
		If IsEles Then
			// 多个元素统计数量
			js = js & "\r\nxWebBox_RetVal=" & VarName & ".length;"
			IsOK, JS_Ret = 运行JS_带运行库(hWin, js)
			If IsOK Then
				Return CLng(JS_Ret), true
			Else
				Return 0, false
			End If
		Else
			// 单个元素找到了就返回 1
			js = js & "\r\nxWebBox_RetVal=isNull(" & VarName & ");"
			IsOK, JS_Ret = 运行JS_带运行库(hWin, js)
			If IsOK Then
				If CBool(JS_Ret) Then
					Return 1, true
				Else
					Return 0, false
				End If
			Else
				Return 0, false
			End If
		End If
	Else
		Return 0, false
	End If
End Function

// 点击元素
Function 元素_点击(hWin, sPath)
	Return 元素_运行JS(hWin, sPath, "{$ele}.click();")
End Function

// 修改元素的 Value
Function 元素_设置内容(hWin, sPath, sVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.click();{$ele}.focus();{$ele}.value = """ & sVal & """;")
End Function

// 获取元素的 Value
Function 元素_获取内容(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.value;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 修改元素的 outerHTML
Function 元素_设置源码(hWin, sPath, sVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.outerHTML = '" & sVal & "';")
End Function

// 获取元素的 outerHTML
Function 元素_获取源码(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.outerHTML;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 修改元素的 innerHTML
Function 元素_设置内部源码(hWin, sPath, sVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.innerHTML = '" & sVal & "';")
End Function

// 获取元素的 innerHTML
Function 元素_获取内部源码(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.innerHTML;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 获取元素的 outerText
Function 元素_获取文本(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.outerText;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 获取元素的 innerText
Function 元素_获取内部文本(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.innerText;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 修改元素的选择状态
Function 元素_设置选择状态(hWin, sPath, bVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.checked=" & iif(CBool(bVal), "true", "false") & ";")
End Function

// 获取元素的选择状态
Function 元素_获取选择状态(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.checked;")
	If IsOK Then
		If IsEles Then
			Return false, false
		Else
			Return CBool(JS_Ret), true
		End If
	Else
		Return false, false
	End If
End Function

// 按索引修改元素的选择状态 [下拉列表]
Function 元素_按索引设置列表选择(hWin, sPath, nIdx)
	Return 元素_运行JS(hWin, sPath, "{$ele}.options[" & nIdx & "].selected = true;if ({$ele}.onchange){{$ele}.onchange()};{$ele}.blur();")
End Function

// 按文本修改元素的选择状态 [下拉列表]
Function 元素_按文本设置列表选择(hWin, sPath, sText)
	Return 元素_运行JS(hWin, sPath, "Select_Item_Text({$ele}, '" & sText & "');")
End Function

// 按Value修改元素的选择状态 [下拉列表]
Function 元素_设置列表选择值(hWin, sPath, sVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.value='" & sVal & "';")
End Function

// 获取列表元素的选择Value
Function 元素_获取列表选择值(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.value;")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 修改元素属性
Function 元素_设置属性(hWin, sPath, sAttr, sVal)
	Return 元素_运行JS(hWin, sPath, "{$ele}.setAttribute('" & sAttr & "', '" & sVal & "');")
End Function

// 获取元素属性
Function 元素_获取属性(hWin, sPath, sAttr)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xWebBox_RetVal = {$ele}.getAttribute('" & sAttr & "');")
	If IsOK Then
		If IsEles Then
			Return ""
		Else
			Return JS_Ret
		End If
	Else
		Return ""
	End If
End Function

// 获取元素位置
Function 元素_获取位置(hWin, sPath)
	Dim IsOK, js, IsEles, VarName, JS_Ret
	IsOK, js, IsEles, VarName, JS_Ret = 元素_运行JS(hWin, sPath, "xyPath_GetPos = {$ele}.getBoundingClientRect();xWebBox_RetVal = xyPath_GetPos.left.toString() + ',' + xyPath_GetPos.top.toString() + ',' + xyPath_GetPos.right.toString() + ',' + xyPath_GetPos.bottom.toString();")
	If IsOK Then
		If IsEles Then
			Return false, 0, 0, 0, 0
		Else
			Dim RetArr = Split(JS_Ret, ",")
			Return true, CLng(RetArr(1)), CLng(RetArr(2)), CLng(RetArr(3)), CLng(RetArr(4))
		End If
	Else
		Return false, 0, 0, 0, 0
	End If
End Function

// 元素截图
Function 元素_截图(hWin, sPath, sFile)
	// 修正位置 [使元素显示]
	Dim IsOK, x1, y1, x2, y2, IsScroll, IsSize, sx, sy, w, h, pw, ph
	Do
		IsOK, x1, y1, x2, y2 = 元素_获取位置(hWin, sPath)
		If IsOK Then
			If (x1 < 0) or (y1 < 0) Then
				// TracePrint "需要修正位置 : " & x1 & " , " & y1
				IsOK, sx, sy = 获取滚动条位置(hWin)
				IsScroll = true
				设置滚动条位置(hWin, sx + x1, sy + y1)
			Else
				Exit Do
			End If
		Else
			Return false
		End If
	Loop
	// 修正大小 [使页面可以显示元素]
	Do
		IsOK, x1, y1, x2, y2 = 元素_获取位置(hWin, sPath)
		If IsOK Then
			w = (x2 - x1) + 24
			h = (y2 - y1) + 24
			IsOK, pw, ph = 获取网页大小(hWin)
			If (pw < w) or (ph < h) Then
				// TracePrint "需要修正大小 : " & w & " , " & h & " (" & pw & " , " & ph & ")"
				IsSize = true
				SendMessage(hWin, &H1502 /* WB_WEBSIZETEMP = WM_USER + &H1102 */ , w, h)
			Else
				Exit Do
			End If
		Else
			Return false
		End If
	Loop
	// 截图
	IsOK, x1, y1, x2, y2 = 元素_获取位置(hWin, sPath)
	If IsOK Then
		元素_截图 = 区域截图(hWin, x1, y1, x2 - x1, y2 - y1, sFile)
	Else
		Return false
	End If
	// 恢复位置和大小
	If IsSize Then
		SendMessage(hWin, &H5, 0, 0)
	End If
	If IsScroll Then
		设置滚动条位置(hWin, sx, sy)
	End If
End Function
