[General]
SyntaxVersion=2
MacroID=140b2dbb-5d27-4d7b-a3d2-d8abf04ef25a
[Comment]

[Script]
 
//------------------------------- API声明 -------------------------------//

Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Function SendMessageS Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long
Declare Function SendMessageP Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByRef wParam As Long, ByRef lParam As Long) As Long
Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Declare Function SetWindowPos Lib "user32" Alias "SetWindowPos" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Declare Function GetClientRect Lib "user32.dll" (ByVal hWnd As Long, ByVal lpRect As Any) As Long
Declare Function GetWindowRect Lib "user32.dll" (ByVal hWnd As Long, ByVal lpRect As Any) As Long
Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hWnd As Long, ByRef lpClassName As String, ByVal nMaxCount As Long) As Long
Declare Function ShowWindow Lib "user32" Alias "ShowWindow" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
Declare Function InvalidateRect Lib "user32" Alias "InvalidateRect" (ByVal hwnd As Long, ByVal lpRect As Long, ByVal bErase As Long) As Long
Declare Function UpdateWindow Lib "user32" Alias "UpdateWindow" (ByVal hwnd As Long) As Long





//------------------------------- 控件属性 -------------------------------//

// 刷新控件
Sub 通用_刷新控件(句柄)
	InvalidateRect(句柄, null, true)
	UpdateWindow(句柄)
End Sub

// 控件样式
// GWL_STYLE = -16
Function 通用_获取样式(句柄)
	Return GetWindowLong(句柄, -16)
End Function
Sub 通用_设置样式(句柄, 样式)
	SetWindowLong(句柄, -16, 样式)
	通用_刷新控件(句柄)
End Sub
Sub 通用_添加样式(句柄, 样式)
	Dim tmpStyle
	tmpStyle = 通用_获取样式(句柄)
	tmpStyle = tmpStyle BOr 样式
	通用_设置样式(句柄, tmpStyle)
	通用_刷新控件(句柄)
End Sub
Sub 通用_删除样式(句柄, 样式)
	Dim tmpStyle
	tmpStyle = 通用_获取样式(句柄)
	tmpStyle = tmpStyle BAND BNot(样式)
	通用_设置样式(句柄, tmpStyle)
	通用_刷新控件(句柄)
End Sub


// 控件扩展样式
// GWL_EXSTYLE = -20
Function 通用_获取扩展样式(句柄)
	Return GetWindowLong(句柄, -20)
End Function
Sub 通用_设置扩展样式(句柄, 样式)
	SetWindowLong(句柄, -20, 样式)
End Sub
Sub 通用_添加扩展样式(句柄, 样式)
	Dim tmpStyle
	tmpStyle = 通用_获取扩展样式(句柄)
	tmpStyle = tmpStyle BOr 样式
	通用_设置扩展样式(句柄, tmpStyle)
	通用_刷新控件(句柄)
End Sub
Sub 通用_删除扩展样式(句柄, 样式)
	Dim tmpStyle
	tmpStyle = 通用_获取扩展样式(句柄)
	tmpStyle = tmpStyle BAnd BNot(样式)
	通用_设置扩展样式(句柄, tmpStyle)
	通用_刷新控件(句柄)
End Sub

// 控件位置\大小
// SWP_NOSIZE		= 1
// SWP_NOMOVE		= 2
// SWP_NOZBOrDER	= 4
// SWP_NOACTIVATE 	= 16
Function 通用_获取宽高(句柄)
	Dim TempRect = StructPack("llll", 0, 0, 0, 0)
	GetWindowRect(句柄, TempRect)
	Dim left, top, right, bottom
	left, top, right, bottom = StructUnPack("llll", TempRect)
	Return right - left, bottom - top
End Function
Function 通用_获取矩形(句柄)
	Dim TempRect = StructPack("llll", 0, 0, 0, 0)
	GetWindowRect(句柄, TempRect)
	Return StructUnPack("llll", TempRect)
End Function
Sub 通用_设置宽高(句柄, 宽, 高)
	SetWindowPos(句柄, 0, 0, 0, 宽, 高, 2 BOr 4 BOr 16)
End Sub
Sub 通用_设置位置(句柄, 左坐标, 上坐标)
	SetWindowPos(句柄, 0, 左坐标, 上坐标, 0, 0, 1 BOr 4 BOr 16)
End Sub
Function 通用_获取位置(句柄)
	Dim left, top, right, bottom
	left, top, right, bottom = 通用_获取矩形(句柄)
	Return left, top
End Function

// 控件标题
// WM_SETTEXT		= 12
// WM_GETTEXT		= 13
// WM_GETTEXTLENGTH = 14
Sub 通用_设置标题(句柄, 标题)
	SendMessageS(句柄, 12, 0, 标题)
End Sub
Function 通用_获取标题(句柄)
	Dim TempInt = SendMessage(句柄, 14, 0, 0)
	Dim TempStr = String(TempInt, "a")
	SendMessageS(句柄, 13, TempInt + 1, TempStr)
	Return TempStr
End Function

// 控件附加数据
// GWL_USERDATA = -21
Function 通用_获取附加数据(句柄)
	Return GetWindowLong(句柄, -21)
End Function
Sub 通用_设置附加数据(句柄, 附加数据)
	SetWindowLong(句柄, -21, 附加数据)
End Sub

// 控件类名
Function 通用_获取类名(句柄)
	Dim Temp = String(260, " ")
	GetClassName(句柄, Temp, 260)
	Return Temp
End Function

// 控件Z序
// HWND_TOPMOST 	= -1
// SWP_NOSIZE		= 1
// SWP_NOMOVE		= 2
// SWP_NOACTIVATE	= 16
Sub 通用_设置置顶(句柄)
	SetWindowPos(句柄, -1, 0, 0, 0, 0, 1 BOr 2 BOr 16)
End Sub
Sub 通用_设置Z序(句柄, 顺序)
	SetWindowPos(句柄, 顺序, 0, 0, 0, 0, 1 BOr 2 BOr 16)
End Sub

// 控件可用状态 [0 不可用]
// WS_DISABLED = 134217728
Function 通用_获取可用状态(句柄)
	Dim TmpStyle
	TmpStyle = 通用_获取样式(句柄)
	If (TmpStyle BAnd 134217728) = 0 Then 
		Return true
	Else 
		Return false
	End If
End Function

// 滚动
// SB_TOP		= 6
// SB_BOTTOM	= 7
// WM_VSCROLL	= 277
Sub 通用_滚动至顶(句柄)
	SendMessage(句柄, 277, 6, 0)
End Sub
Sub 通用_滚动至底(句柄)
	SendMessage(句柄, 277, 7, 0)
End Sub






//------------------------------- 窗口命令 -------------------------------//

// 窗口显示\隐藏
// SW_SHOW 	= 5
// SW_HIDE	= 0
Sub 窗口_显示(句柄)
	ShowWindow(句柄, 5)
End Sub
Sub 窗口_隐藏(句柄)
	ShowWindow(句柄, 0)
End Sub
Sub 窗口_设置显示状态(句柄, 状态)
	ShowWindow(句柄, 状态)
End Sub

// 窗口状态 [0=正常  1=最小化  2=最大化]
// SW_MAXIMIZE = 3
// SW_MINIMIZE = 6
// SW_RESTORE  = 9
// WS_MAXIMIZE = 16777216
// WS_MINIMIZE = 536870912
Function 窗口_获取状态(句柄)
	Dim TmpStyle
	TmpStyle = 通用_获取样式(句柄)
	If (TmpStyle BAnd 16777216) <> 0 Then 
		Return 2
	ElseIf (TmpStyle BAnd 536870912) <> 0 Then
		Return 1
	Else 
		Return 0
	End If
End Function
Sub 窗口_设置状态(句柄, 状态)
	Dim TmpStyle
	TmpStyle = 通用_获取样式(句柄)
	Select Case 状态
		Case 1
			ShowWindow(句柄, 6)
		Case 2
			ShowWindow(句柄, 3)
		Case Else
			ShowWindow(句柄, 9)
	End Select
End Sub











//------------------------------- 按钮命令 -------------------------------//

// 按钮点击
// BM_CLICK = 245
Sub 按钮_点击(句柄)
	SendMessage(句柄, 245, 0, 0)
End Sub










//------------------------------- 多选框命令 -------------------------------//

// 选中
// BM_GETCHECK = 240
// BM_SETCHECK = 241
Function 多选框_获取状态(句柄)
	Return Iif(SendMessage(句柄, 240, 0, 0) > 0, true, false)
End Function
Sub 多选框_设置状态(句柄, 状态)
	SendMessage(句柄, 241, 状态, 0)
End Sub










//------------------------------- 单选框命令 -------------------------------//

// 选中
Function 单选框_获取状态(句柄)
	Return Iif(SendMessage(句柄, 240, 0, 0) > 0, true, false)
End Function
Sub 单选框_设置状态(句柄, 状态)
	SendMessage(句柄, 241, 状态, 0)
End Sub











//------------------------------- 输入框命令 -------------------------------//

// 文本
Function 输入框_获取文本(句柄)
	Return 通用_获取标题(句柄)
End Function
Sub 输入框_设置文本(句柄, 文本)
	通用_设置标题(句柄, 文本)
End Sub

// 只读 [0: 未锁定]
// EM_SETREADONLY	= 207
// ES_READONLY		= 2048
Function 输入框_获取锁定状态(句柄)
	Dim TempStyle = 通用_获取样式(句柄)
	Return Iif((TempStyle BAnd 2048) = 0, false, true)
End Function
Sub 输入框_设置锁定状态(句柄, 状态)
	If 状态 Then
		SendMessage(句柄, 207, true, 0)
	Else
		SendMessage(句柄, 207, false, 0)
	End If
End Sub

// 获取长度
// WM_GETTEXTLENGTH = 14
Function 输入框_获取长度(句柄)
	Return SendMessage(句柄, 14, 0, 0)
End Function

// 是否修改
// EM_GETMODIFY = 184
// EM_SETMODIFY = 185
Function 输入框_获取修改状态(句柄)
	Return Iif(SendMessage(句柄, 184, 0, 0) = 0, false, true)
End Function
Sub 输入框_设置修改状态(句柄, 状态)
	SendMessage(句柄, 185, 状态, 0)
End Sub

// 复制
// WM_COPY = 769
Sub 输入框_复制(句柄)
	SendMessage(句柄, 769, 0, 0)
End Sub
// 剪切
// WM_CUT = 768
Sub 输入框_剪切(句柄)
	SendMessage(句柄, 768, 0, 0)
End Sub
// 粘贴
// WM_PASTE = 770
Sub 输入框_粘贴(句柄)
	SendMessage(句柄, 770, 0, 0)
End Sub
// 清除
// WM_CLEAR = 771
Sub 输入框_清除(句柄)
	SendMessage(句柄, 771, 0, 0)
End Sub
// 全选
// EM_SETSEL = 177
Sub 输入框_全选(句柄)
	SendMessage(句柄, 177, 0, -1)
End Sub
// 撤销
// EM_UNDO = 199
Sub 输入框_撤销(句柄)
	SendMessage(句柄, 199, 0, 0)
End Sub
// 是否可以撤销
// EM_UNDO = 199
Function 输入框_是否可以撤销(句柄)
	Return Iif(SendMessage(句柄, 199, 0, 0) = 0, false, true)
End Function

// 选择长度
// EM_GETSEL = 176
// EM_SETSEL = 177
Function 输入框_获取选择长度(句柄)
	Dim SelStart, SelEnd
	SendMessageP(句柄, 176, SelStart, SelEnd)
	Return SelEnd - SelStart
End Function
Sub 输入框_设置选择长度(句柄, 长度)
	Dim SelStart, SelEnd
	SendMessageP(句柄, 176, SelStart, SelEnd)
	SendMessage(句柄, 177, SelStart, SelStart + 长度)
End Sub

// 选择开头
Function 输入框_获取开始位置(句柄)
	Dim SelStart, SelEnd
	SendMessageP(句柄, 176, SelStart, SelEnd)
	Return SelStart
End Function
Sub 输入框_设置开始位置(句柄, 开始位置)
	Dim SelStart, SelEnd
	SendMessage(句柄, 177, 开始位置, 开始位置)
End Sub

// 选择结尾
Function 输入框_获取结尾位置(句柄)
	Dim SelStart, SelEnd
	SendMessageP(句柄, 176, SelStart, SelEnd)
	Return SelEnd
End Function
Sub 输入框_设置结尾位置(句柄, 结尾位置)
	Dim SelStart, SelEnd
	SelStart = 输入框_获取开始位置(句柄)
	SendMessage(句柄, 177, SelStart, 结尾位置)
End Sub

// 选择文字
// EM_REPLACESEL = 194
Function 输入框_获取选择文字(句柄)
	Dim SelStart, SelLen, Text
	SelStart = 输入框_获取开始位置(句柄)
	SelLen = 输入框_获取选择长度(句柄)
	Text = 输入框_获取文本(句柄)
	Return Mid(Text, SelStart + 1, SelLen)
End Function
Sub 输入框_设置选择文字(句柄, 文字)
	SendMessageS(句柄, 194, true, 文字)
End Sub










//------------------------------- 列表框命令 -------------------------------//

// 列表项内容
// LB_GETTEXT		= 393
// LB_GETTEXTLEN	= 394
Function 列表框_获取项内容(句柄, 索引)
	Dim ItemLen, Item
	ItemLen = SendMessage(句柄, 394, 索引, 0)
	If ItemLen > 0 Then 
		Item = String(ItemLen + 1, " ")
		SendMessageS(句柄, 393, 索引, Item)
		Return Item
	End If
End Function
Sub 列表框_设置项内容(句柄, 索引, 内容)
	Dim ItemData
	ItemData = 列表框_获取附加数据(句柄, 索引)
	列表框_删除项(句柄, 索引)
	列表框_插入项(句柄, 索引, 内容)
	列表框_设置附加数据(句柄, 索引, ItemData)
End Sub

// 列表项附加数据
// LB_GETITEMDATA	= 409
// LB_SETITEMDATA	= 410
Function 列表框_获取附加数据(句柄, 索引)
	Return SendMessage(句柄, 409, 索引, 0)
End Function
Sub 列表框_设置附加数据(句柄, 索引, 附加数据)
	SendMessage(句柄, 410, 索引, 附加数据)
End Sub

// 添加列表项
// LB_ADDSTRING		= 384
// LB_INSERTSTRING	= 385
Sub 列表框_添加项(句柄, 内容)
	列表框_添加项增强(句柄, 内容, "")
End Sub
Sub 列表框_添加项增强(句柄, 内容, 附加数据)
	Dim 索引
	索引 = SendMessageS(句柄, 384, 0, 内容)
	列表框_设置附加数据(句柄, 索引, 附加数据)
End Sub
Sub 列表框_插入项(句柄, 索引, 内容)
	SendMessageS(句柄, 385, 索引, 内容)
End Sub

// 删除项
// LB_DELETESTRING = 386
// LB_RESETCONTENT = 388
Sub 列表框_删除项(句柄, 索引)
	SendMessage(句柄, 386, 索引, 0)
End Sub
Sub 列表框_清空项(句柄)
	SendMessage(句柄, 388, 0, 0)
End Sub

// 选中
// LB_SETSEL = 389
// LB_GETSEL = 391
// LB_GETSELCOUNT = 400
// LB_GETSELITEMS = 401
Function 列表框_获取选中状态(句柄, 索引)
	Return Iif(SendMessage(句柄, 391, 索引, 0) > 0, true, false)
End Function
Sub 列表框_设置多选(句柄, 索引, 是否选中)
	SendMessage(句柄, 389, 是否选中, 索引)
End Sub
Function 列表框_获取选中数量(句柄)
	Return SendMessage(句柄, 400, 0, 0)
End Function
/*  暂时无法实现
Function 列表框_清除选中(句柄)
	Dim ItemCount = 列表框_获取选中数量(句柄)
	Dim SelIdx = String(ItemCount * 4)
	SendMessageLS(句柄, 401, ItemCount, SelIdx)
	TracePrint SelIdx
	TracePrint Len(SelIdx)
	TracePrint String(ItemCount, "l")
	TracePrint StructUnPack(String(ItemCount, "l"), SelIdx)
	'Return Array()
End Function
*/

// 列表数量
// LB_GETCOUNT = 395
Function 列表框_获取项数量(句柄)
	Return SendMessage(句柄, 395, 0, 0)
End Function

// 获取当前项
// LB_SETCURSEL = 390
// LB_GETCURSEL = 392
Function 列表框_获取当前项(句柄)
	Return SendMessage(句柄, 392, 0, 0)
End Function
Sub 列表框_设置当前项(句柄, 索引)
	SendMessage(句柄, 390, 索引, 0)
End Sub

// 获取当前项内容
Function 列表框_获取当前项内容(句柄)
	Dim ItemIdx
	ItemIdx = 列表框_获取当前项(句柄)
	Return 列表框_获取项内容(句柄, ItemIdx)
End Function
Sub 列表框_设置当前项内容(句柄, 内容)
	Dim ItemIdx
	ItemIdx = 列表框_获取当前项(句柄)
	列表框_设置项内容(句柄, ItemIdx, 内容)
End Sub











//------------------------------- 下拉框命令 -------------------------------//


// 下拉框内容
// CB_GETLBTEXT		= 328
// CB_GETLBTEXTLEN	= 329
Function 下拉框_获取项内容(句柄, 索引)
	Dim ItemLen, Item
	ItemLen = SendMessage(句柄, 329, 索引, 0)
	If ItemLen > 0 Then 
		Item = String(ItemLen + 1, " ")
		SendMessageS(句柄, 328, 索引, Item)
		Return Item
	End If 
End Function
Sub 下拉框_设置项内容(句柄, 索引, 内容)
	Dim ItemData
	ItemData = 下拉框_获取附加数据(句柄, 索引)
	下拉框_删除项(句柄, 索引)
	下拉框_插入项(句柄, 索引, 内容)
	下拉框_设置附加数据(句柄, 索引, ItemData)
End Sub

// 附加数据
// CB_GETITEMDATA	= 336
// CB_SETITEMDATA	= 337
Function 下拉框_获取附加数据(句柄, 索引)
	Return SendMessage(句柄, 336, 索引, 0)
End Function
Sub 下拉框_设置附加数据(句柄, 索引, 附加数据)
	SendMessage(句柄, 337, 索引, 附加数据)
End Sub

// 添加下拉框项
// CB_ADDSTRING		= 323
// CB_INSERTSTRING	= 330
Sub 下拉框_添加项(句柄, 内容)
	下拉框_添加项增强(句柄, 内容, 0)
End Sub
Sub 下拉框_添加项增强(句柄, 内容, 附加数据)
	Dim 索引
	索引 = SendMessageS(句柄, 323, 0, 内容)
	下拉框_设置附加数据(句柄, 索引, 附加数据)
End Sub
Sub 下拉框_插入项(句柄, 索引, 内容)
	SendMessageS(句柄, 330, 索引, 内容)
End Sub

// 删除下拉框项
// CB_DELETESTRING = 324
// CB_RESETCONTENT = 331
Sub 下拉框_删除项(句柄, 索引)
	SendMessage(句柄, 324, 索引, 0)
End Sub
Sub 下拉框_清空项(句柄)
	SendMessage(句柄, 331, 0, 0)
End Sub

// 项数量
// CB_GETCOUNT = 326
Function 下拉框_获取项数量(句柄)
	Return SendMessage(句柄, 326, 0, 0)
End Function

// 当前项
// CB_GETCURSEL = 327
// CB_SETCURSEL = 334
Function 下拉框_获取当前项(句柄)
	Return SendMessage(句柄, 327, 0, 0)
End Function
Sub 下拉框_设置当前项(句柄, 索引)
	SendMessage(句柄, 334, 索引, 0)
End Sub

// 获取当前项
Function 下拉框_获取当前项内容(句柄)
	Dim ItemIdx
	ItemIdx = 下拉框_获取当前项(句柄)
	Return 下拉框_获取项内容(句柄, ItemIdx)
End Function
Sub 下拉框_设置当前项内容(句柄, 内容)
	Dim ItemIdx
	ItemIdx = 下拉框_获取当前项(句柄)
	下拉框_设置项内容(句柄, ItemIdx, 内容)
	下拉框_设置当前项(句柄, ItemIdx)
End Sub




